--[[
	General constants
]]

-- Development
DISABLE_AUDIO = true
PRODUCTION = true
START_IN_LEVEL = true
START_LEVEL = "desert"

-- Controls
ARROW_DOWN = 'down'
ARROW_LEFT = 'left'
ARROW_RIGHT = 'right'
ARROW_UP = 'up'
BUTTON_A = 'a'
BUTTON_B = 'b'
BUTTON_X = 'x'
BUTTON_Y = 'y'
BUTTON_PAUSE = 'pause'
KEY_F11 = 'f11'

-- Game screen size
GAME_FULLSCREEN = false
GAME_HEIGHT = 600
GAME_WIDTH = 800

-- Saving
CONFIG_FILE = "config.txt"
SAVE_FILE = "save.txt"

-- Timing
BGM_FADE = 1        -- Time it will take to fade between BGM sounds
DT_LIMIT = 1 / 15   -- Biggest math calculation step possible (15th of a second)