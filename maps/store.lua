return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "1.0.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 160,
  height = 30,
  tilewidth = 10,
  tileheight = 10,
  nextobjectid = 97,
  properties = {},
  tilesets = {
    {
      name = "fg",
      firstgid = 1,
      tilewidth = 1600,
      tileheight = 300,
      spacing = 0,
      margin = 0,
      image = "../gfx/hell/fg.png",
      imagewidth = 1600,
      imageheight = 300,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 1600,
        height = 300
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "bg_1",
      firstgid = 2,
      tilewidth = 1600,
      tileheight = 300,
      spacing = 0,
      margin = 0,
      image = "../gfx/hell/bg.png",
      imagewidth = 1600,
      imageheight = 300,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 1600,
        height = 300
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "space",
      firstgid = 3,
      tilewidth = 400,
      tileheight = 300,
      spacing = 0,
      margin = 0,
      image = "../gfx/hell/space.png",
      imagewidth = 400,
      imageheight = 300,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 400,
        height = 300
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "sign",
      firstgid = 4,
      tilewidth = 41,
      tileheight = 65,
      spacing = 0,
      margin = 0,
      image = "../gfx/hell/sign.png",
      imagewidth = 41,
      imageheight = 65,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 41,
        height = 65
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "holland",
      firstgid = 5,
      tilewidth = 17,
      tileheight = 24,
      spacing = 0,
      margin = 0,
      image = "../gfx/hell/holland.png",
      imagewidth = 17,
      imageheight = 24,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 17,
        height = 24
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    },
    {
      name = "door_1",
      firstgid = 6,
      tilewidth = 60,
      tileheight = 75,
      spacing = 0,
      margin = 0,
      image = "../gfx/hell/door_1.png",
      imagewidth = 60,
      imageheight = 75,
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 60,
        height = 75
      },
      properties = {},
      terrains = {},
      tilecount = 1,
      tiles = {}
    }
  },
  layers = {
    {
      type = "objectgroup",
      name = "portals",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 96,
          name = "door_2",
          type = "",
          shape = "rectangle",
          x = 259.333,
          y = 244.667,
          width = 34.6667,
          height = 13.6667,
          rotation = 0,
          visible = true,
          properties = {
            ["to"] = "shack_2:door_2"
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "bg_2",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 4,
          name = "bg_2",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 300,
          width = 400,
          height = 300,
          rotation = 0,
          gid = 3,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "bg_1",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 31,
          name = "bg_1",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 300,
          width = 1600,
          height = 300,
          rotation = 0,
          gid = 2,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "fg",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 1,
          name = "fg",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 300,
          width = 1600,
          height = 300,
          rotation = 0,
          gid = 1,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "object",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 28,
          name = "holland",
          type = "",
          shape = "rectangle",
          x = 930,
          y = 232.5,
          width = 17,
          height = 24,
          rotation = 0,
          gid = 5,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "collision",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 33,
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 10,
          height = 300,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 34,
          name = "",
          type = "",
          shape = "rectangle",
          x = 10,
          y = 290,
          width = 1590,
          height = 10,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 35,
          name = "",
          type = "",
          shape = "rectangle",
          x = 1590,
          y = 0,
          width = 10,
          height = 290,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 46,
          name = "",
          type = "",
          shape = "rectangle",
          x = 10,
          y = 0,
          width = 181,
          height = 250,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 48,
          name = "",
          type = "",
          shape = "rectangle",
          x = 293,
          y = 165,
          width = 107,
          height = 80,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 49,
          name = "",
          type = "",
          shape = "rectangle",
          x = 400,
          y = 160,
          width = 31,
          height = 90,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 56,
          name = "",
          type = "",
          shape = "rectangle",
          x = 718,
          y = 169,
          width = 83,
          height = 81,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 59,
          name = "",
          type = "",
          shape = "rectangle",
          x = 834,
          y = 169,
          width = 756,
          height = 81,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 61,
          name = "",
          type = "",
          shape = "rectangle",
          x = 431,
          y = 207,
          width = 111,
          height = 43,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 69,
          name = "",
          type = "",
          shape = "rectangle",
          x = 537,
          y = 177,
          width = 5,
          height = 30,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 70,
          name = "",
          type = "",
          shape = "rectangle",
          x = 529,
          y = 124,
          width = 5,
          height = 83,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 71,
          name = "",
          type = "",
          shape = "rectangle",
          x = 534,
          y = 124,
          width = 69,
          height = 85,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 72,
          name = "",
          type = "",
          shape = "rectangle",
          x = 191,
          y = 0,
          width = 1398,
          height = 168,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 74,
          name = "",
          type = "",
          shape = "rectangle",
          x = 801,
          y = 169,
          width = 33,
          height = 81,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 75,
          name = "",
          type = "",
          shape = "rectangle",
          x = 592,
          y = 209,
          width = 4,
          height = 41,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 76,
          name = "",
          type = "",
          shape = "rectangle",
          x = 597,
          y = 210,
          width = 93,
          height = 40,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 77,
          name = "",
          type = "",
          shape = "rectangle",
          x = 690,
          y = 209,
          width = 28,
          height = 41,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 79,
          name = "",
          type = "",
          shape = "rectangle",
          x = 191,
          y = 168,
          width = 102,
          height = 77,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 80,
          name = "",
          type = "",
          shape = "rectangle",
          x = 191,
          y = 245,
          width = 12,
          height = 5,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "stuff",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 86,
          name = "sign",
          type = "",
          shape = "rectangle",
          x = 1334,
          y = 262,
          width = 41,
          height = 65,
          rotation = 0,
          gid = 4,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "speakers",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 89,
          name = "hell_sign",
          type = "",
          shape = "rectangle",
          x = 1341,
          y = 201,
          width = 32,
          height = 88,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 90,
          name = "hell_store",
          type = "",
          shape = "rectangle",
          x = 540,
          y = 210,
          width = 60,
          height = 20,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 93,
          name = "hell_shack",
          type = "",
          shape = "rectangle",
          x = 261,
          y = 245.667,
          width = 31.6667,
          height = 11,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "door",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 32,
          name = "door_1",
          type = "",
          shape = "rectangle",
          x = 536.014,
          y = 209.006,
          width = 60,
          height = 75,
          rotation = 0,
          gid = 6,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "important",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 29,
          name = "player",
          type = "",
          shape = "rectangle",
          x = 1499,
          y = 269,
          width = 10,
          height = 10,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
