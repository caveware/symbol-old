-- Load all fonts

return {
	map = love.graphics.newFont(... .. '/Bitter-Regular.otf', 16),
	menu_item = love.graphics.newFont(... .. '/Bitter-Regular.otf', 32),
	notification = love.graphics.newFont(... .. '/Bitter-Regular.otf', 40),
	text = love.graphics.newFont(... .. '/Bitter-Regular.otf', 18),
	text_italic = love.graphics.newFont(... .. '/Bitter-Italic.otf', 18),
	timer = love.graphics.newFont(... .. '/Bitter-Regular.otf', 24)
}