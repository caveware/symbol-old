-- Load SFX

return {
	bing = love.audio.newSource(... .. '/bing.ogg'),
	blip = love.audio.newSource(... .. '/blip.ogg'),
	door_close = love.audio.newSource(... .. '/door_close.ogg'),
	door_knock = love.audio.newSource(... .. '/door_knock.ogg'),
	door_open = love.audio.newSource(... .. '/door_open.ogg'),
	hit = love.audio.newSource(... .. '/hit.ogg'),
	spawn = love.audio.newSource(... .. '/spawn.ogg'),
	start = love.audio.newSource(... .. '/start.ogg'),
	startup = love.audio.newSource(... .. '/startup.ogg')
}