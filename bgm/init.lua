--[[
	Loads all the background music for the game
]]

local bgm = {
	levels = require("src.config.bgm"),
	list = { }
}

-- Loads a BGM file into the system
local function addBGM(file)
	bgm.list[file] = love.audio.newSource("bgm/" .. file .. ".ogg")
	bgm.list[file]:setLooping(true)
end

-- Load all background music
addBGM('badwater')
addBGM('desert')
addBGM('fire')
addBGM('intro')
addBGM('main_menu')
addBGM('overlook')
addBGM('store')

-- Play a song
function bgm:play(file)
	local bgm = self.list

	if not self.current then
		self.current = file
		bgm[file]:setVolume(1)
		bgm[file]:play()
	elseif self.current ~= file then
		if self.phase then
			bgm[self.current]:stop()
			self.current = self.phase.next
		end
		self.phase = { next = file, time = 0 }
		if self.current then bgm[self.current]:setVolume(1) end
		bgm[self.phase.next]:setVolume(0)
		bgm[self.phase.next]:play()
	end
end

-- Play the BGM for a level
function bgm:playLevel(level)
	self:play(self.levels[level])
end

-- Stop the currently playing BGM
function bgm:stop()
	local bgm = self.list

	if self.phase then
		if self.current then bgm[self.current]:stop() end
		self.current = self.phase.next
		self.phase.next = nil
		self.phase.time = 0
	elseif self.current then
		self.phase = { time = 0 }
	end
end

-- Update the BGM system
function bgm:update(dt)
	local bgm = self.list

	if self.phase then
		local ratio = self.phase.time / BGM_FADE
		self.phase.time = self.phase.time + dt

		-- Update volumes
		if self.current then bgm[self.current]:setVolume(1 - ratio) end
		if self.phase.next then bgm[self.phase.next]:setVolume(ratio) end

		if self.phase.time >= BGM_FADE then
			if self.current then bgm[self.current]:stop() end
			self.current = self.phase.next
			if self.current then bgm[self.current]:setVolume(1) end
			self.phase = nil
		end
	end
end

return bgm