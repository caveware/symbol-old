--[[
	Store state
]]

local items = require "src.config.items"

local Store = {}

function Store:enter(last)
	-- Save last state
	self.last = last

	-- Create canvas
	self.canvas = love.graphics.newCanvas(GAME_WIDTH, GAME_HEIGHT)

	-- Fade the state in
	self.fade = 0
	Timer.tween(1, self, {
		fade = 1
	}, 'in-out-sine', function ()
		self.fade = 1
	end)

	-- Set up BGM
	self.last_bgm = bgm.current
	bgm:play("store")

	-- Items to show
	self.items = {}
	for id, v in pairs(items) do
		if v.in_store then
			table.insert(self.items, {
				description = v.description,
				id = id,
				name = v.name
			})
		end
	end

	self.scroll = 0
	self.selected = Vector(1, 1)
end

function Store:getCurrent()
	return (self.selected.y - 1) * 3 + self.selected.x
end

function Store:select(item)
	self._item = item
end

function Store:update(dt)
	if self.fade < 1 then return end

	-- Select item
	local item = self.items[self:getCurrent()]
	if item then
		self.scroll = math.min(self.scroll + dt * 160, #item.description)

		if Input:pressed(BUTTON_A) then
			self:select(item)
		end
	end

	-- Fade the state out if pressing B
	if Input:pressed(BUTTON_B) then
		Timer.tween(1, self, {
			fade = 0
		}, 'in-out-sine', function ()
			-- Leave state
			bgm:play(self.last_bgm)
			Gamestate.pop()
		end)
	end

	-- Change current selection
	if Input:pressed(ARROW_LEFT) then
		self.selected.x = math.max(1, self.selected.x - 1)
		self.scroll = 0
	end
	if Input:pressed(ARROW_RIGHT) then
		self.selected.x = math.min(3, self.selected.x + 1)
		self.scroll = 0
	end
	if Input:pressed(ARROW_UP) then
		self.selected.y = math.max(1, self.selected.y - 1)
		self.scroll = 0
	end
	if Input:pressed(ARROW_DOWN) then
		self.selected.y = math.min(3, self.selected.y + 1)
		self.scroll = 0
	end
end

function Store:draw()
	local item = self.items[self:getCurrent()]

	-- Save old canvas
	local old_canvas = love.graphics.getCanvas()
	-- Draw store to our canvas
	love.graphics.setCanvas(self.canvas)

	-- Background
	love.graphics.setColor(0, 0, 0)
	love.graphics.rectangle("fill", 0, 0, GAME_WIDTH, GAME_HEIGHT)
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(gfx.store.ui)

	-- Items
	local ix, iy, iw, ih = 393, 14, 131, 131
	for k, item in ipairs(self.items) do
		local id = (k - 1)
		local x = id % 3
		local y = math.floor(id / 3)
		love.graphics.draw(gfx.store[item.id], ix + x * iw, iy + y * ih)

		if _.find(Game.inventory, item.id) then
			love.graphics.draw(gfx.store.unavailable, ix + x * iw, iy + y * ih)
		end
	end

	-- Cursor
	local cx, cy = ix + (self.selected.x - 1) * iw, iy + (self.selected.y - 1) * iw
	love.graphics.draw(gfx.store.cursor, cx, cy)

	-- Item info
	love.graphics.setFont(fnt.text)
	if item and self.fade == 1 then
		local description = item.description:sub(1, math.floor(self.scroll))
		love.graphics.print(item.name, 24, 498)
		love.graphics.printf(description, 24, 524, 746, "left")
	end

	-- Draw to old canvas
	love.graphics.setCanvas(old_canvas)
	self.last:draw()
	love.graphics.setColor(255, 255, 255, self.fade * 255)
	love.graphics.draw(self.canvas)

	-- Show opening dialogue if first time at store
	if not Game.flags.store_visited and self.fade == 1 then
		Game.flags.store_visited = true
		Gamestate.push(states.Text, txt.speakers.store_initial)
	end

	-- Show shop dialogue
	if self._item then
		local id = _.find(Game.inventory, item.id)
			and "unavailable"
			or self._item.id
		Gamestate.push(states.Text, txt.speakers["store_" .. id])
		self._item = nil
	end
end

return Store