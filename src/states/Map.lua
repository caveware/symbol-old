--[[
	Map which shows on-screen
]]

local Map = {
	selected = "desert"
}

function Map:enter(previous_state)
	-- Defaults
	self.chosen = false
	self.fade = false
	self.last = previous_state
	if _.find(Game.levels, Game.level) then
		self.selected = Game.level
	end

	-- Opening timer fade
	self.timer = 0
	Timer.tween(.25, self, {
		timer = 1
	}, 'in-out-sine', function ()
		timer = 1
	end)

	self.icons = {
		cave = {
			down = "desert",
			left = "store",
			right = "overlook",
			x = 254,
			y = 67,
			text = "A cave in the ranges adorned with ancient petroglyphic art."
		},
		desert = {
			down = "sinkhole",
			left = "store",
			right = "overlook",
			up = "cave",
			x = 168,
			y = 104,
			text = "The site of remote shack where Murray resides."
		},
		overlook = {
			down = "sinkhole",
			left = "desert",
			up = "cave",
			x = 263,
			y = 152,
			text = "Dante's View, an overlook that looms over the Badwater basin."
		},
		sinkhole = {
			left = "store",
			right = "overlook",
			up = "desert",
			x = 142,
			y = 192,
			text = "The sinkhole in Badwater that forms the soul's well."
		},
		store = {
			down = "desert",
			left = "sinkhole",
			right = "cave",
			x = 132,
			y = 67,
			text = "Hell, the community of the survivors of the soul's well."
		}
	}
end

function Map:update(dt)
	if self.timer < 1 then return end

	-- Performed upon pressing A
	if Input:pressed(BUTTON_A) then
		self.chosen = true

		-- Fade out if going to new level
		if self.selected == Game.level then
			self:quit()
			return
		end
		sfx.bing:play()
		self.fade = true


		Timer.tween(1, self, {
			timer = 0
		}, 'in-out-sine', function ()
			Gamestate.pop()

			if self.fade then
				Gamestate.switch(states.Wandering, self.selected)
			end
		end)
	end

	-- Go back if pressing button
	if Input:pressed(BUTTON_B) then
		self:quit()
	end

	-- Movement between icons
	if Input:pressed(ARROW_DOWN) then
		self:move(ARROW_DOWN)
	elseif Input:pressed(ARROW_LEFT) then
		self:move(ARROW_LEFT)
	elseif Input:pressed(ARROW_RIGHT) then
		self:move(ARROW_RIGHT)
	elseif Input:pressed(ARROW_UP) then
		self:move(ARROW_UP)
	end
end

function Map:move(dir)
	local next = self.icons[self.selected][dir]

	if next and _.find(Game.levels, next) then
		self.selected = next
		sfx.blip:play()
	end
end

function Map:quit()
	Timer.tween(.25, self, {
		timer = 0
	}, 'in-out-sine', function ()
		Gamestate.pop()
	end)
end

function Map:draw()
	local offset = (love.timer.getTime() * .6) % 1
	self.last:draw()

	-- Draw fade in
	love.graphics.setColor(0, 0, 0, 128 * self.timer)
	love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

	love.graphics.scale(2, 2)

	-- Draw the map background
	local y = 300 * (1 - self.timer)
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(gfx.map.blank, 0, y)

	-- Draw each icon which should be shown
	for k, level in ipairs(Game.levels) do
		local x = self.icons[level].x + 10
		local y_offset = self.icons[level].y + 10
		local icon = gfx.map.icon[level]

		-- Draw flashing icon if required
		if level == self.selected then
			love.graphics.setColor(128, 128, 128, (1 - offset) * 128)
			love.graphics.draw(icon, x, y + y_offset, 0, 1 + offset, 1 + offset, 10, 10)
		end

		-- Draw straight-up icon
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(icon, x, y + y_offset, 0, 1, 1, 10, 10)
	end

	love.graphics.scale(.5, .5)

	-- Draw the selected text
	love.graphics.setColor(0, 0, 0)
	love.graphics.setFont(fnt.map)
	love.graphics.printf(self.icons[self.selected].text, 225, (y + 264) * 2, 350, 'center')

	-- Draw fade out
	if self.fade then
		love.graphics.setColor(0, 0, 0, 255 * (1 - self.timer))
		love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)
	end
end

return Map