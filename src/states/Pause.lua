--[[
	Pause state
]]

local PauseState = {}

function PauseState:enter(last_state)
	-- Store previous state
	self.last_state = last_state
	self.selected = 1

	-- Fade in state
	self.fade = 0
	Timer.tween(.25, self, {
		fade = 1
	}, 'in-out-sine', function ()
		self.fade = 1
	end)

	-- Options
	self.options = {
		{
			name = "Resume Game"
		},
		{
			name = "Options"
		},
		{
			name = "Back to Main Menu"
		},
		{
			name = "Quit Game"
		}
	}
end

function PauseState:update(dt)
	-- Do not execute when fading
	if self.fade < 1 then return end

	-- Close pause menu
	if Input:pressed(BUTTON_B) or Input:pressed(BUTTON_PAUSE) then
		self:resumeGame()
	end

	-- Move the selection
	if Input:pressed(ARROW_UP) then
		self.selected = self.selected - 1
		if self.selected == 0 then
			self.selected = #self.options
		end
	end
	if Input:pressed(ARROW_DOWN) then
		self.selected = self.selected + 1
		if self.selected == 5 then
			self.selected = 1
		end
	end

	-- Select option
	if Input:pressed(BUTTON_A) then
		if self.selected == 1 then
			self:resumeGame()
		elseif self.selected == 3 then
			Gamestate.pop()
			Gamestate.switch(states.MainMenu)
		elseif self.selected == 4 then
			love.event.quit()
		end
	end
end

function PauseState:resumeGame()
	Timer.tween(.25, self, {
		fade = 0
	}, 'in-out-sine', function ()
		self.fade = 0
		Gamestate.pop()
	end)
end

function PauseState:draw()
	-- Draw last state
	self.last_state:draw()

	-- Draw fade
	love.graphics.setColor(0, 0, 0, 128 * self.fade)
	love.graphics.rectangle("fill", 0, 0, GAME_WIDTH, GAME_HEIGHT)

	-- Draw UI
	love.graphics.setColor(255, 255, 255, 255 * self.fade)
	love.graphics.setFont(fnt.notification)
	love.graphics.printf("PAUSED", 0, 200, GAME_WIDTH, "center")
	love.graphics.setFont(fnt.text)
	for k, option in ipairs(self.options) do
		local text = option.name
		local width = love.graphics.getFont():getWidth(text)
		local x = GAME_WIDTH / 2
		local y = 300 + (k - 1) * 25
		love.graphics.printf(text, 0, y, GAME_WIDTH, "center")

		if self.selected == k then
			love.graphics.rectangle("line", x - width / 2 - 2, y, width + 4, 25)
		end
	end
end

return PauseState