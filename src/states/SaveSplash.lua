--[[
	Splash for autosaving
]]

local SaveSplash = {}

function SaveSplash:enter()
	self.timer = 0
	Timer.tween(3, self, {
		timer = 1
	}, 'in-out-sine', function ()
		Timer.tween(3, self, {
			timer = 0
		}, 'in-out-sine', function ()
			Gamestate.push(states.MainMenu)
		end)
	end)
end

function SaveSplash:draw()
	local alpha = math.clamp(0, self.timer * 20, 1)
	love.graphics.setColor(255, 255, 255, 255 * alpha)
	love.graphics.draw(gfx.splash.save)
end

return SaveSplash