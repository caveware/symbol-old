--[[
	Loads all states
]]

return {
	CDSplash = require(... .. '/CDSplash'),
	Intro = require(... .. '/Intro'),
	Inventory = require(... .. '/Inventory'),
	MainMenu = require(... .. '/MainMenu'),
	Map = require(... .. '/Map'),
	Pause = require(... .. '/Pause'),
	PickUpItem = require(... .. '/PickUpItem'),
	SaveSplash = require(... .. '/SaveSplash'),
	Store = require(... .. '/Store'),
	Text = require(... .. '/Text'),
	Wandering = require(... .. '/Wandering')
}