--[[
	Inventory
]]

-- Load all items
local items = require "src.config.items"

local Inventory = {
	selected = 0
}

function Inventory:enter(previous_state)
	self.last = previous_state
	self.timer = 0
	self.selected = 0
	self.selected_powers = false

	self.items = {}
	self.powers = {}
	for key, item in pairs(items) do
		if _.find(Game.inventory, key) then
			if item.in_store then
				table.insert(self.powers, _.extend(item, { id = key }))
			else
				table.insert(self.items, _.extend(item, { id = key }))
			end
		end
	end

	Timer.tween(.75, self, {
		timer = 1
	}, 'in-out-sine', function ()
		self.timer = 1
	end)
end

function Inventory:update(dt)
	if self.timer < 1 then return end

	local item = self.items[self.selected + 1]
	if Input:pressed(BUTTON_A) and item and not self.selected_powers then
		self.item_chose = item.id
	end

	if Input:pressed(BUTTON_B) or Input:pressed(BUTTON_Y ) then
		Timer.tween(.75, self, {
			timer = 0
		}, 'in-out-sine', Gamestate.pop)
	end

	if Input:pressed("left") then
		local val = self.selected_powers and 5 or 4
		local rows = math.floor(self.selected / val)
		self.selected = (self.selected - 1) % val + rows * 4
	end

	if Input:pressed("right") then
		local val = self.selected_powers and 5 or 4
		local rows = math.floor(self.selected / val)
		self.selected = (self.selected + 1) % val + rows * 4
	end

	if Input:pressed("up") then
		if self.selected_powers then
			self.selected_powers = false
			self.selected = math.floor(self.selected / 4 * 3) + 4
		else
			if self.selected < 4 then
				self.selected = math.round(self.selected / 3 * 4)
				self.selected_powers = true
			else
				self.selected = (self.selected - 4) % 8
			end
		end
	end

	if Input:pressed("down") then
		if self.selected_powers then
			self.selected_powers = false
			self.selected = math.floor(self.selected / 4 * 3)
		else
			if self.selected > 3 then
				self.selected = math.round((self.selected - 4) / 3 * 4)
				self.selected_powers = true
			else
				self.selected = (self.selected + 4) % 8
			end
		end
	end
end

function Inventory:draw()
	self.last:draw()
	love.graphics.setFont(fnt.text)

	-- Draw fade in background
	love.graphics.setColor(0, 0, 0, 128 * self.timer)
	love.graphics.rectangle("fill", 0, 0, GAME_WIDTH, GAME_HEIGHT)

	-- Draw items
	love.graphics.setColor(255, 255, 255, 255 * self.timer)
	love.graphics.draw(gfx.inventory.ui)
	for key, item in ipairs(self.items) do
		local x = 260 + ((key - 1) % 4) * 131
		local y = 214 + math.floor((key - 1) / 4) * 131
		love.graphics.draw(gfx.inventory[item.id], x, y)

		if key == (self.selected + 1) and not self.selected_powers then
			love.graphics.print(item.name, 25, 158)
			love.graphics.printf(item.description, 25, 205, 206)
		end
	end

	-- Draw powers
	for key, item in ipairs(self.powers) do
		local x = 352 + (key - 1) * 68
		local y = 476
		love.graphics.draw(gfx.inventory[item.id], x, y)

		if key == (self.selected + 1) and self.selected_powers then
			love.graphics.print(item.name, 25, 158)
			love.graphics.printf(item.description, 25, 205, 206)
		end
	end

	-- Draw cursor
	if self.selected_powers then
		local x = 352 + self.selected * 68
		local y = 476
		love.graphics.draw(gfx.inventory.small_cursor, x, y)
	else
		local x = 260 + (self.selected % 4) * 131
		local y = 214 + math.floor(self.selected / 4) * 131
		love.graphics.draw(gfx.inventory.cursor, x, y)
	end

	-- Draw shards and night
	love.graphics.printf("Night " .. Game.night, 260, 158, 259, "center")
	love.graphics.printf("$" .. Game.shards, 522, 158, 259, "center")

	-- Show item
	if self.item_chose then
		Gamestate.push(states.PickUpItem, self.item_chose, true)
		self.item_chose = nil
	end
end

return Inventory