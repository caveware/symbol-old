--[[
	State to show text on screen
]]

local Text = Class({})

function Text:enter(previous_state, text)
	self.fade = 1
	self.last = previous_state
	self.text = text

	-- Set current glyph to change each .1 seconds
	self.glyph = RandomGlyph()
	self.glyph_timer = Timer.every(.1, function ()
		self.glyph:update()
	end)

	-- Backgrounds
	self.lastBG = {
		img = "blank",
		scroll = { }
	}
	self.bg = text[1].bg or {
		img = "blank",
		scroll = { }
	}
	if not self.bg.scroll then
		self.bg.scroll = { }
	end

	-- BGM
	if text[1].bgm then bgm:play(text[1].bgm) end
	if text[1].sfx then sfx[text[1].sfx]:play() end

	-- Current option
	self.option = 0

	-- Current phase of the text rollout
	self.phase = 0

	-- Current stage of the text process
	self.stage = 1

	-- Update flags
	self:applyFlags()

	-- Run code if required
	if self.text.code then
		self.text = self.text.code()
	end
end

function Text:applyFlags()
	local stage = self.text[self.stage]

	if stage.flags then
		for key, value in pairs(stage.flags) do
			Game.flags[key] = value
		end
	end
end

function Text:leave()
	Timer.cancel(self.glyph_timer)
end

function Text:update(dt)
	if self.fade < 1 then return end

	local stage = self.text[self.stage]
	local text = stage.code and stage.code() or stage.text
	if stage.bg then
		self.bg = stage.bg
		if not self.bg.scroll then
			self.bg.scroll = { }
		end
	end

	-- Perform actions if 'a' is pressed
	if Input:pressed(BUTTON_A) then
		if self.phase < #text then
			self.phase = #text
		elseif self.stage < #self.text or stage.choices or stage.next then
			if stage.choices then
				self.stage = 1
				self.text = stage.choices[self.option + 1].to
			elseif stage.next then
				self.stage = 1
				self.text = stage.next
			else
				self.stage = self.stage + 1
			end

			if self.text.code then
				self.text = self.text.code()
			end

			-- Update flags
			self:applyFlags()

			-- Update background
			self.lastBG = self.bg

			-- Update BGM
			local song = self.text[self.stage].bgm
			local effect = self.text[self.stage].sfx
			if song then bgm:play(song) end
			if effect then sfx[effect]:play() end

			-- Reset options
			self.phase = 0
			self.option = 0
		else
			Timer.tween(.5, self, {
				fade = 0
			}, 'in-out-sine', function ()
				self.leave_state = true
			end)
		end
	end

	-- Update phase if not at end
	if self.phase < #text then
		self.phase = math.min(self.phase + 160 * dt, #text)
		if (not sfx.blip:isPlaying()) then
			sfx.blip:setVolume(.8 + .2 * math.random())
			sfx.blip:setPitch(.95 + .1 * math.random())
			sfx.blip:play()
		end
	end

	-- Update current choice
	if self.phase == #text and stage.choices then
		if Input:pressed('down') then
			self.option = (self.option + 1) % #stage.choices
		end
		if Input:pressed('up') then
			self.option = (self.option - 1) % #stage.choices
		end
	end
end

function Text:drawBG(bg, alpha)
	local image = gfx.dialog.bg[bg.img]
	local scale = bg.scale
	local time = love.timer.getTime()
	local offset = {
		x = (time * (bg.scroll.x or 0)) % image:getWidth(),
		y = (time * (bg.scroll.y or 0)) % image:getHeight()
	}

	-- Draw scroll outers [[ SCALE PROP NOT YET SUPPORTED ]]
	love.graphics.setColor(255, 255, 255, 255 * alpha)
	if offset.x ~= 0 then
		love.graphics.draw(image, offset.x - image:getWidth(), offset.y)
		if offset.y ~= 0 then
			love.graphics.draw(image, offset.x - image:getWidth(), offset.y - image:getHeight())
		end
	end
	if offset.y ~= 0 then
		love.graphics.draw(image, offset.x, offset.y - image:getHeight())
	end

	-- Draw main element
	love.graphics.draw(image, offset.x, offset.y, 0, scale)
end

function Text:draw()
	local stage = self.text[self.stage]
	local text = stage.code and stage.code() or stage.text
	local text_x = stage.speaker and 134 or 25
	local text_width = stage.speaker and 641 or 750
	self.last:draw()
	love.graphics.setFont(stage.italics and fnt.text_italic or fnt.text)

	-- Draw background
	local alpha = self.bg.cut and 1 or (self.phase / #stage.text)
	if self.lastBG == self.bg then
		alpha = 1
	end
	self:drawBG(self.lastBG, math.min(1, (2 - alpha * 2) * self.fade))
	self:drawBG(self.bg, math.min(alpha * 2, 1 * self.fade))

	-- Draw choices
	if stage.choices then
		love.graphics.setColor(0, 0, 0, 255 * self.fade)
		love.graphics.rectangle('fill', 120, 250, 400, #stage.choices * 40 + 10)
		love.graphics.setColor(33, 33, 33, 255 * self.fade)
		love.graphics.rectangle('fill', 129, 259 + 38 * self.option, 382, 35)
		love.graphics.setColor(255, 255, 255, 255 * self.fade)
		love.graphics.setLineWidth(2)
		love.graphics.rectangle('line', 120, 250, 400, #stage.choices * 40 + 10)
		for key, opt in ipairs(stage.choices) do
			love.graphics.print(opt.text, 135, 264 + 38 * (key - 1))
		end
	end

	-- Draw panel
	love.graphics.setColor(255, 255, 255, 255 * self.fade)
	if stage.speaker then
		love.graphics.draw(gfx.ui.textbox_image)

		-- Draw speaker
		local speaker = speakers[stage.speaker]
		local icon = (self.phase ~= #self.text)
			and math.floor((self.phase % 20) * #speaker.icon / 20) + 1
			or 1
		love.graphics.draw(speaker.icon[icon], 17, 492)
	else
		love.graphics.draw(gfx.ui.textbox)
	end

	-- Draw text
	local cutoff_text = text:sub(1, math.floor(self.phase))
	love.graphics.printf(cutoff_text, text_x, 500, text_width, 'left')

	-- Draw glyphs if text rolled out
	if self.stage ~= #self.text then
		self.glyph:draw(750, 555)
	end

	-- Leave state if required
	if self.leave_state then
		self.leave_state = false
		Gamestate.pop()
	end
end

return Text