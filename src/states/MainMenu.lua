--[[
	Main menu for the game
]]

local MainMenu = {}

function MainMenu:enter()
	-- Play the background music
	bgm:play('main_menu')

	-- Alpha for fading in and out
	self.alpha = 0

	-- Fade in at state start
	Timer.tween(1.5, self, {
		alpha = 1
	}, 'sine', function () self.alpha = 1 end)

	-- Menu options
	self.item = 0
	self.items = love.filesystem.exists(SAVE_FILE) and 4 or 3
end

function MainMenu:update(dt)
	-- Interpret pressing up and down
	local mod = 0
	if Input:pressed "down" then mod = 1 end
	if Input:pressed "up" then mod = -1 end
	self.item = (self.item + mod) % self.items

	-- Interpret pressing enter
	if self.alpha == 1 and Input:pressed "a" and (self.items == 4 and (self.item ~= 2) or (self.item ~= 1)) then
		if self.item < self.items - 2 then
			sfx.bing:play()
		end
		bgm:stop()

		Timer.tween(1.5, self, {
			alpha = 0
		}, 'sine', function ()
			if self.item == 0 and self.items == 4 then
				Save:load()
			elseif self.item == self.items - 3 then
				Gamestate.switch(states.Intro)
			elseif self.item == self.items - 1 then
				love.event.quit()
			else
				-- SETTINGS
			end
		end)
	end
end

function MainMenu:draw()
	love.graphics.setColor(255, 255, 255, 192 * self.alpha)

	-- Draw scrolling carving
	local offset = (love.timer.getTime() * 200) % 1200
	love.graphics.draw(gfx.menu.carving, 0, -offset)
	love.graphics.draw(gfx.menu.carving, 0, 1200 - offset)

	-- Draw bg text
	love.graphics.draw(self.items == 4 and gfx.menu.text_cont or gfx.menu.text)

	-- Draw line before selected option
	love.graphics.rectangle('fill', 64, 378 + self.item * 34.5, 2, 18)
end

return MainMenu