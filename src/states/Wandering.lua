--[[
	State for wandering around the landscape
]]

-- Classes
local WanderingPlayer = require "src.classes.WanderingPlayer"

local Wandering = {}

function Wandering:init()
	-- Set current glyph to change each .1 seconds
	self.glyph = RandomGlyph()
	self.glyph_timer = Timer.every(.1, function ()
		self.glyph:update()
	end)
end

function Wandering:enter(previous_state, level, door, player_position)
	-- Send notification
	NC:send(LEVEL_NAMES[level] or "")

	-- Determine supplementary scripts
	self.scripts = levels[level]

	-- Play the music for the level
	bgm:playLevel(level)

	-- Load the map
	Game.level = level
	self.map = STI("maps/" .. level .. ".lua")
	self.map_height = self.map.height * self.map.tileheight
	self.map_width = self.map.width * self.map.tilewidth

	-- Interpret collisions
	self.collisions = self.map.layers.collision.objects
	self.map:removeLayer("collision")

	-- Interpret portals
	self.portals = nil
	if self.map.layers.portals then
		self.portals = self.map.layers.portals.objects
		self.map:removeLayer("portals")
	end

	-- Interpret speakers
	self.speakers = nil
	if self.map.layers.speakers then
		self.speakers = self.map.layers.speakers.objects
		self.map:removeLayer("speakers")
	end

	-- Position where player will start
	local player_x, player_y = 0, 0

	-- Interpret important layer
	for k, object in ipairs(self.map.layers.important.objects) do
		if object.name == "player" then
			player_x = object.x
			player_y = object.y
		end
	end
	self.map:removeLayer("important")

	-- If door, move to portal
	if door and self.portals then
		for k, portal in ipairs(self.portals) do
			if portal.name == door then
				player_x = portal.x + portal.width / 2 - 16
				player_y = portal.y + portal.height / 2 - 8
			end
		end
	end

	-- Create the player
	self.player = WanderingPlayer(
		player_position or Vector(player_x, player_y)
	)
	Game.player = { x = player_x, y = player_y }

	-- Set up the view
	self.view = Vector(0, 0)

	-- Fade the screen in
	self.fade = 0
	Timer.tween(1, self, {
		fade = 1
	}, 'sine', function () self.fade = 1 end)

	-- Update layer of objects
	self:updateObjectsLayer()

	-- Autosave the game
	Save:save()

	-- Run supplementary script
	self.hide_player = false
	if self.scripts and self.scripts.enter then
		self.scripts.enter(self)
	end
end

function Wandering:getObjects()
	return self.map.layers.object.objects
end

function Wandering:findClosestItemToPlayer()
	-- Determine player's centre
	local px = self.player.position.x + self.player.size.x / 2
	local py = self.player.position.y + self.player.size.y / 2

	-- Find item to utilise
	local item
	for k, object in ipairs(self:getObjects()) do
		local ox = object.x + object.width / 2
		local oy = object.y + object.height / 2

		if math.dist(px, py, ox, oy) < 32 then
			item = {
				x = object.x,
				y = object.y,
				width = object.width,
				height = object.height,
				name = object.name,
				type = "object"
			}
		end
	end

	-- Find portal to utilise
	if not item and self.portals then
		for k, object in ipairs(self.portals) do
			local ox = object.x + object.width / 2
			local oy = object.y + object.height / 2

			if math.dist(px, py, ox, oy) < 32 then
				item = {
					x = object.x,
					y = object.y,
					width = object.width,
					height = object.height,
					name = object.name,
					to = object.properties.to,
					type = "portal"
				}
			end
		end
	end

	-- Find speaker to utilise
	if not item and self.speakers then
		for k, object in ipairs(self.speakers) do
			local ox = object.x + object.width / 2
			local oy = object.y + object.height / 2

			if math.dist(px, py, ox, oy) < 32 then
				item = {
					x = object.x,
					y = object.y,
					width = object.width,
					height = object.height,
					name = object.name,
					type = "speaker"
				}
			end
		end
	end

	return item
end

function Wandering:update(dt)
	if self.fade == 1 and not self.prevent_movement then
		-- Update player
		self.player:update(dt, self.collisions)
		Game.player = {
			x = self.player.position.x,
			y = self.player.position.y
		}

		-- Perform actions if 'a' pressed
		if Input:pressed "a" then
			-- Determine closest, selectable object
			local item = self:findClosestItemToPlayer()

			if self.scripts and self.scripts.interpretItem then
				item = self.scripts.interpretItem(self, item)
			end

			-- Add item to table if found and object
			if item then
				if item.type == "object" then
					table.insert(Game.inventory, item.name)
					self.item = item.name

					-- Update layer of objects
					self:updateObjectsLayer()
				elseif item.type == "portal" then
					local index = item.to:find(":")
					local level = item.to:sub(1, index - 1)
					local door = item.to:sub(index + 1)
					Timer.tween(1, self, {
						fade = 0
					}, 'sine', function ()
						Gamestate.switch(self, level, door)
					end)
				elseif item.type == "speaker" then
					self.open_text = txt.speakers[item.name]
				end
			end
		end

		-- Open map
		if Input:pressed "b" then
			self.open_map = true
		end

		-- Open inventory
		if Input:pressed(BUTTON_Y) then
			self.open_inventory = true
		end

		-- Pause game
		if Input:pressed(BUTTON_PAUSE) then
			self.to_pause = true
		end
	end

	-- Update viewport based on player's position
	self.view.x = math.clamp(
		0,
		self.player.position.x - 196 + (self.view.x_offset or 0),
		self.map_width - 400
	)
	self.view.y = math.clamp(
		0,
		self.player.position.y - 166 + (self.view.y_offset or 0),
		self.map_height - 300
	)

	-- Run supplementary script
	if self.scripts and self.scripts.update then
		self.scripts.update(self, dt)
	end
end

function Wandering:updateObjectsLayer()
	-- Remove all objects in map which have been collected
	local object_table = self:getObjects()
	for i = #object_table, 1, -1 do
		local object = object_table[i]

		if _.detect(Game.inventory, object.name) then
			table.remove(object_table, i)
		end
		self.map:setObjectSpriteBatches(self.map.layers.object)
	end
end

function Wandering:draw()
	local x = math.round(-self.view.x)
	local y = math.round(-self.view.y)
	love.graphics.setColor(255, 255, 255)
	love.graphics.scale(2, 2)

	-- Draw parallax layers
	if self.map.layers.bg_2 then self.map:drawObjectLayer("bg_2") end
	love.graphics.translate(x * .2, y * .2)
	if self.map.layers.bg_1 then self.map:drawObjectLayer("bg_1") end
	love.graphics.translate(x * .8, y * .8)

	-- Draw all non-parallax layers
	for k, layer in ipairs(self.map.layers) do
		if layer.name ~= "bg_1" and layer.name ~= "bg_2" then
			layer:draw(0, 0)
		end
	end

	-- Run supplementary script
	if self.scripts and self.scripts.draw then
		self.scripts.draw(self)
	end

	-- Actually draw the player
	if (not self.hide_player) then self.player:draw() end

	-- Draw icon if can use item
	local item = self:findClosestItemToPlayer()
	if item and not self.prevent_movement then
		self.glyph:draw(item.x + item.width / 2 - 8, item.y - 24, .5)
	end

	-- Reset graphics
	love.graphics.origin()

	-- Run supplementary script
	if self.scripts and self.scripts.drawFG then
		self.scripts.drawFG(self)
	end

	-- Draw cover if fading
	if self.fade < 1 then
		love.graphics.setColor(0, 0, 0, (1 - self.fade) * 255)
		love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)
	end

	-- Move to item state if been picked up
	if self.item then
		Gamestate.push(states.PickUpItem, self.item)
		self.item = false
	end

	-- Open map if button pressed
	if self.open_map then
		Gamestate.push(states.Map)
		self.open_map = false
	end

	-- Open inventory if button pressed
	if self.open_inventory then
		Gamestate.push(states.Inventory)
		self.open_inventory = false
	end

	-- Open text if button pressed
	if self.open_text then
		Gamestate.push(states.Text, self.open_text)
		self.open_text = false
	end

	if self.to_pause then
		Gamestate.push(states.Pause)
		self.to_pause = false
	end
end

return Wandering