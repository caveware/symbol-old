--[[
	Showed when picking up item
]]

local PickUpItem = Class({})

function PickUpItem:enter(previous_state, item, no_text)
	self.item = item
	self.last_state = previous_state
	self.no_text = no_text
	self.timer = 0
	Timer.tween(.75, self, {
		timer = 1
	}, 'in-out-sine', function ()
		Timer.after(.5, function ()
			if not self.no_text then
				Gamestate.push(states.Text, txt.items[self.item])
			end
		end)
	end)
end

function PickUpItem:resume()
	Timer.tween(.75, self, {
		timer = 0
	}, 'in-out-sine', function ()
		if not self.no_text then
			Save:save()
		end
		Gamestate.pop()
	end)
end

function PickUpItem:draw()
	-- Draw last state
	self.last_state:draw()

	-- Draw fade in
	love.graphics.setColor(0, 0, 0, 192 * self.timer)
	love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

	-- Draw item
	love.graphics.setColor(255, 255, 255)
	local x = -900 + (self.timer * 900)
	local y = 550 - (self.timer * 550)
	love.graphics.draw(gfx.items[self.item], x, y, -.4 * (1 - self.timer), 2, 2)

	-- Quit if pressing A
	if Input:pressed(BUTTON_A) and self.no_text and self.timer == 1 then
		self:resume()
	end
end

return PickUpItem