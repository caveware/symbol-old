--[[
	State for the game intro
]]

local Intro = {}

function Intro:init()
	-- Generate quads to split up flashing glyphs
	self.glyphQuads = {}
	for y = 0, 2 do
		for x = 0, 3 do
			local quad = love.graphics.newQuad(x * 16, y * 16, 16, 16, 64, 48)
			table.insert(self.glyphQuads, quad)
		end
	end
end

function Intro:enter()
	self.cover = 1

	-- Glyphs to show
	self.glyphs = {}

	-- Wait 2 seconds then show the intro
	Timer.after(2, function ()
		Gamestate.push(states.Text, txt.intro)
	end)
end

function Intro:resume()
	-- Play BGM
	bgm:play("intro")

	-- Fade in the screen
	Timer.tween(5, self, {
		cover = 0
	}, 'sine', function ()
		self.cover = 0
		self:startGlyphs()
	end)
end

function Intro:fadeOut()
	if self.fading_out then return end

	-- Fade to the main menu
	self.fading_out = true
	Timer.tween(2.5, self, {
		cover = 1
	}, 'sine', function ()
		Gamestate.switch(states.Wandering, "desert")
	end)
end

function Intro:startGlyphs()
	Timer.every(.1, function ()
		for i = 1, 12 do
			self.glyphs[i] = math.random(4) > 3 and math.random(12) or 0
		end
	end)

	Timer.after(25, function ()
		self:fadeOut()
	end)
end

function Intro:update(dt)
	if Input:pressed("a") and self.cover == 0 then
		-- self:fadeOut()
	end
end

function Intro:draw()
	love.graphics.setColor(255, 255, 255, (1 - self.cover) * 255)
	love.graphics.draw(gfx.ui.title)

	-- Draw glyphs
	for key, value in ipairs(self.glyphs) do
		if value ~= 0 then
			local x = math.ceil(key / 2 - 1) * 103 + 135
			local y = 270 + (key % 2) * 70
			love.graphics.draw(gfx.ui.glyphs, self.glyphQuads[value], x, y)
		end
	end
end

return Intro