--[[
	Caveware Digital splash
]]

local CDSplash = {}

function CDSplash:enter()
	sfx.startup:play()

	-- Fade through timer
	self.timer = 0
	Timer.tween(4, self, {
		timer = 3
	}, 'in-out-sine', function ()
		Gamestate.switch(states.SaveSplash)
	end)
end

function CDSplash:draw()
	-- Draw b/w
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(gfx.splash.cd_bw)

	-- Draw colour
	local alpha = 1 - math.clamp(0, (self.timer - 1) * 3, 1)
	love.graphics.setColor(255, 255, 255, alpha * 255)
	love.graphics.draw(gfx.splash.cd_colour)

	-- Draw fading
	local key = (math.abs(self.timer - 1.5) - 1.25) / .25
	love.graphics.setColor(0, 0, 0, 255 * math.clamp(0, key, 1))
	love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)
end

return CDSplash