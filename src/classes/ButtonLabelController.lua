--[[
	Controls buttons with labels
]]

local ButtonLabel = require "src.classes.ButtonLabel"

local ButtonLabelController = Class({})

return ButtonLabelController