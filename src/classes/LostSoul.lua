--[[
	Lost Soul
]]

local LostSoul = Class({})

function LostSoul:init(position)
	self.position = position or Vector(0, 0)
end

function LostSoul:update(dt, player_pos)
	self.dir = (player_pos - self.position):normalized()
	self.position.x = self.position.x + self.dir.x * dt * 20
	self.position.y = self.position.y + self.dir.y * dt * 20
end

function LostSoul:draw()
	local key = math.floor((love.timer.getTime() % 1) * 2) + 1

	local dir = (self.dir:toPolar().x * 180 / math.pi) % 360
	local image_name = "down"
	if (dir > 45) then image_name = "right" end
	if (dir > 135) then image_name = "up" end
	if (dir > 215) then image_name = "left" end
	if (dir > 305) then image_name = "down" end

	local x, y = self.position:unpack()
	love.graphics.draw(gfx.shadow[image_name .. key], x - 16, y - 48)
end

return LostSoul