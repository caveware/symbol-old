--[[
	Random glyph which can be drawn
]]

local RandomGlyph = Class({})

function RandomGlyph:init()
	self:update()
end

function RandomGlyph:update()
	local last = self.glyph
	while self.glyph == last do
		self.glyph = math.floor(math.random() * 16 + 1)
	end
end

function RandomGlyph:draw(x, y, scale)
	local quad = symbolQuads[math.ceil(self.glyph / 4)][self.glyph % 4 + 1]
	love.graphics.draw(gfx.ui.symbols2, quad, x, y, 0, scale or 1)
end

return RandomGlyph