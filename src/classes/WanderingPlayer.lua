--[[
	Player in the wandering state
]]

local WanderingPlayer = Class({
	animation_phase = 0,
	damage = 0,
	direction = Vector(0, 0),
	invincibility = 0,
	position = Vector(0, 0),
	size = Vector(32, 16),
	speed = Vector(80, 60)
})

-- Performed upon player creation
function WanderingPlayer:init(position, size)
	self.position = position or self.position
	self.size = size or self.size
end

function WanderingPlayer:getDamage()
	return self.damage
end

-- Returns the player position
function WanderingPlayer:getPosition()
	return self.position:unpack()
end

function WanderingPlayer:isInvincible()
	return self.invincibility > 0
end

function WanderingPlayer:inflictDamage()
	if self.damage < 2 and not self:isInvincible() then
		self.damage = self.damage + 1
		self.invincibility = 1
		sfx.hit:play()
		return true
	else
		return false
	end
end

-- Returns the collision hitbox for the player
function WanderingPlayer:toHitbox()
	return {
		x = self.position.x,
		y = self.position.y,
		height = self.size.y,
		width = self.size.x
	}
end

-- Updates the player
function WanderingPlayer:update(dt, collisions)
	-- Update invincibility
	self.invincibility = math.max(0, self.invincibility - dt)

	local hit, hitbox
	local speed = self.speed

	-- Remember positioning
	local previous = self.position:clone()

	-- Determine movement speed
	local move_x = Input:get("right") - Input:get("left")
	local move_y = Input:get("down") - Input:get("up")
	self.direction = Vector(move_x, move_y):normalizeInplace()

	-- Move horizontally
	self.position.x = self.position.x + self.direction.x * dt * speed.x
	hitbox = self:toHitbox()
	hit = Hit.checkRectangleTable(hitbox, collisions)
	if hit then
		self.position.x = self.direction.x > 0
			and hit.x - self.size.x
			or hit.x + hit.width
	end

	-- Move vertically
	self.position.y = self.position.y + self.direction.y * dt * speed.y
	hitbox = self:toHitbox()
	hit = Hit.checkRectangleTable(hitbox, collisions)
	if hit then
		self.position.y = self.direction.y > 0
			and hit.y - self.size.y
			or hit.y + hit.height
	end

	-- Update phase
	if self.direction:len() == 0 or self.position == previous then
		self.animation_phase = 0
	else
		self.animation_phase = (self.animation_phase + dt * 6) % 2
	end
end

-- Draws the player
function WanderingPlayer:draw()
	local x, y = self.position:unpack()
	local width, height = self.size:unpack()

	-- Determines the image to show
	local imageset = gfx.player[self.damage + 1]
	local image = imageset.idle
	if self.direction:len() > 0 then
		local dir
		if math.abs(self.direction.x) > math.abs(self.direction.y) then
			dir = self.direction.x > 0 and 'right' or 'left'
		else
			dir = self.direction.y > 0 and 'down' or 'up'
		end

		image = imageset[dir .. math.floor(self.animation_phase + 1)]
	end

	-- Draws the player
	love.graphics.setColor(255, 255, 255, 255 * (self:isInvincible() and 128 or 255))
	love.graphics.draw(image, math.round(x), math.round(y) - 48)
	love.graphics.setColor(255, 255, 255)
end

return WanderingPlayer