--[[
	Android controls
]]

local AndroidControls = Class({
	deadzone = .3
})

function AndroidControls:init(controls, deadzone)
	-- Settings
	self.analog = {}
	self.controls = controls
	self.deadzone = deadzone or self.deadzone
	self.touches = {}

	-- Contains all control data
	self.data = {
		left = 0,
		right = 0,
		up = 0,
		down = 0,
		a = 0,
		b = 0,
		x = 0,
		y = 0
	}
	self.last = {}

	-- Create buttons
	self:setupButtons()
end

function AndroidControls:get(key)
	return self.data[key] or 0
end

function AndroidControls:pressed(key	)
	local data = self.data[key] or 0
	local last = self.last[key] or 0
	return data
		and (data > self.deadzone and last <= self.deadzone)
		or false
end

function AndroidControls:setupButtons()
	local controls = {
		'x', 'y', 'b', 'a'
	}
	local h = love.graphics.getHeight()
	local gw = h * 4 / 3
	local w = (love.graphics.getWidth() - gw) / 2
	local x = love.graphics.getWidth() - w
	
	self.buttons = {}
	for i = 0, 3 do
		table.insert(self.buttons, {
			x = x,
			y = i * (h / 4),
			width = w,
			height = h / 4,
			control = controls[i + 1]
		})
	end
end

function AndroidControls:update(dt)
	local h = love.graphics.getHeight()
	local scale = h / GAME_HEIGHT
	local w = love.graphics.getWidth()
	local touches = love.touch.getTouches()

	for k, v in pairs(self.data) do
		self.last[k] = v
	end

	-- Remove defunct touches
	for i = #self.touches, 1, -1 do
		local found = false
		for k, v in ipairs(touches) do
			if v == self.touches[i].id then
				found = true
			end
		end
		if not found then
			local touch = self.touches[i]
			if touch.ox < w / 2 then
				self.data.down = 0
				self.data.left = 0
				self.data.right = 0
				self.data.up = 0
			elseif touch.ctrl then
				self.data[touch.ctrl] = 0
			end
			table.remove(self.touches, i)
		end
	end

	-- Interpret active touches
	for i = #touches, 1, -1 do
		local id = touches[i]
		local touch = _.findWhere(self.touches, {
			id = id
		})
		local x, y = love.touch.getPosition(id)

		if touch then
			touch.x = x
			touch.y = y
		else
			local t = {
				id = id,
				ox = x,
				oy = y,
				x = x,
				y = y
			}
			local ctrl = Hit.checkRectangleTable({
				x = x,
				y = y,
				width = 0,
				height = 0
			}, self.buttons)
			if ctrl then
				t.ctrl = ctrl.control
				self.data[t.ctrl] = 1
			end
			table.insert(self.touches, t)
		end
	end

	-- Update data
	for k, v in ipairs(self.touches) do
		local vx = (v.x - v.ox) / (300 * scale)
		local vy = (v.y - v.oy) / (300 * scale)
		if v.ox < w / 2 then
			self.data.left = math.clamp(0, -vx, 1)
			self.data.right = math.clamp(0, vx, 1)
			self.data.up = math.clamp(0, -vy, 1)
			self.data.down = math.clamp(0, vy, 1)
		end
	end
end

function AndroidControls:draw()
	love.graphics.setColor(255, 255, 255)

	for k, v in ipairs(self.buttons) do
		love.graphics.rectangle("line", v.x, v.y, v.width, v.height)
		love.graphics.print(v.control, v.x + 16, v.y + 16)
	end
end

return AndroidControls