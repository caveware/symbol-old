--[[
	Specific button with a label
]]

local ButtonLabel = Class({
	x = 0,
	y = 0
	text = "cool"
})

function ButtonLabel:init(x, y, text)
	self.x = x or self.x
	self.y = y or self.y
	self.text = text or self.text
end

function ButtonLabel:draw()
	love.graphics.print(self.text, self.x, self.y)
end

return ButtonLabel