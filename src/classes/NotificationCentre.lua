--[[
	Displays notifications on screen.
]]

local NotificationCentre = Class({
	timer = 3
})

function NotificationCentre:init(timer)
	self.phase = 0
	self.queue = {}
	self.timer = timer or self.timer
end

function NotificationCentre:update(dt)
	-- Increase phase if items to show
	if #self.queue > 0 then self.phase = self.phase + dt end

	-- Update if phase is past timer
	if self.phase >= self.timer then
		self.phase = 0
		table.remove(self.queue, 1)
	end
end

function NotificationCentre:send(text)
	table.insert(self.queue, text)
end

function NotificationCentre:draw()
	if #self.queue > 0 then
		local font = fnt.notification
		love.graphics.setFont(font)

		local halftime = self.timer / 2
		local phase = math.clamp(0, (halftime - math.abs(self.phase - halftime)) * 4, 1)
		local alpha = phase * 255

		-- Positioning
		local x, y = 32, -32 + 64 * phase
		local width, height = font:getWidth(self.queue[1]) + 16, font:getHeight() + 16

		-- Draw notification
		love.graphics.setColor(0, 0, 0, alpha)
		love.graphics.rectangle("fill", x - 8, y - 8, width, height)
		love.graphics.setColor(255, 255, 255, alpha)
		love.graphics.rectangle("line", x - 8, y - 8, width, height)
		love.graphics.print(self.queue[1], x, y)
	end
end

return NotificationCentre