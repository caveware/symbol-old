--[[
	Manages saving the game
]]

local SaveManager = Class({
	is_saving = false,
	timer = 0
})

function SaveManager:load(data)
	local data = love.filesystem.read(SAVE_FILE)
	Game = JSON.decode(data)
	Gamestate.switch(states.Wandering, Game.level, nil, Vector(Game.player.x, Game.player.y))
end

function SaveManager:save()
	Timer.tween(1, self, {
		timer = 1
	}, 'sine', function () self.timer = 1 end)
	self.is_saving = true
	love.filesystem.write(SAVE_FILE, JSON.encode(Game))
	self.is_saving = false
end

function SaveManager:update()
	if not self.is_saving and self.timer == 1 then
		Timer.tween(1, self, {
			timer = 0
		}, 'sine', function () self.timer = 0 end)
	end
end

return SaveManager