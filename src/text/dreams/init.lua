--[[
	Loads all random dreams
]]

return {
	require(... .. '/01'),
	require(... .. '/02')
}