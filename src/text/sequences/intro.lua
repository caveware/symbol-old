--[[
	Text for the intro of the game
]]

return {
	{text = "Out here, the desert speaks to me."},
	{text = "There is not a soul here and yet I hear voices carried upon the wind."},
	{text = "This is what they tell me."},
	{
		bg = {
			img = "intro1",
			scroll = {
				x = 100,
				y = 50
			}
		},
		bgm = "fire",
		text = "There was a time when this"
	},
	{ text = "In this time, there were folk who occupied this place in their nomadic movements across the land."},
	{ text = "Those folk left unique petroglyphic art upon many stones here that tell of a people that sustained a fairly prosperous tribe that had been stable for a reasonably long time." },
	{
		bg = {
			img = "intro2",
			scroll = {
				x = 50,
				y = 100
			}
		},
		text = "But, at some point in their history, a cataclysm led to a gradual but irreversible collapse of their culture."
	},
	{ text = "We know of this for many reasons, to which we can thank the discipline of archaeology."},
	{ text = "One of the main reasons was that the event was represented by the people themselves, in their rock art."},
	{ text = "This event was not a social or economic crisis. No, in fact, it was something quite different."},
	{ text = "It was an event of profound spiritual significance that ultimately caused much destruction and suffering to the inhabitants of this area."},
	{ text = "A cataclysm of the soul..."},
	{ text = "..."},
	{ text = "I'm sure all this grandoise spiritual talk must seem a little, well...a little meaningless."},
	{ text = "I would imagine that the truth would leave many people incredulous. The rest of the story is hard to believe..."},
	{ text = "I would have thought the same thing many years ago. I wish I could return to that innocence."},
	{ text = "You said you were someone of faith, but I'm not sure that can exactly prepare you..."},
	{ text = "Which is why, before I continue, I'd like you to see the place upon which all of this happened with your own eyes."},
	{ text = "And you'll witness for yourself the fate of the people here, and the hell it has wrought upon this place."},
	{
		bg = {
			cut = true,
			img = "blank"
		},
		text = "This place..."
	},
	{ text = "This desert..."},
	{ text = "It really does contain some terrible secrets."},
	{ text = "And tonight, we are going to witness them first hand..."},

}
