--[[
	Text when picking up the postcard
]]

return {
	{ text = "It's an antiquated-looking picture postcard littered on the desert floor. It looks like it was discarded here long ago." },
	{ text = "The text reads, 'Greetings From Death Valley, California'. The back of the postcard is blank." },
	{ text = "The front features images of some imposing basins and mountain areas, presumably of the Death Valley area. The place has a striking resemblance to the formations I can see in the distance." },
	{ text = "Hm...Death Valley, California..." },
	{ text = "I think I'll take this." }
}