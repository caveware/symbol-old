--[[
	Text when picking up the postcard
]]

return {
	{ text = "Some kind of advertisment for a general store on a little slip of paper." },
	{ text = "I can't tell if this is particularly old or not. Who knows how long this has been sitting here, decaying under the intense sun of the desert?" },
	{ text = "No idea why this slip is sitting so close to the sinkhole. Maybe its owner tried throwing trash into it." },
	{ text = "Or maybe they accidentally fell in. It wouldn't be too hard to believe. There aren't any safety ropes or barriers here." },
}