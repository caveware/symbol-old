--[[
	Text when picking up the photograph
]]

return {
	{ text = "Wh-?" },
	{ text = "Haha, what's this doing out here?" },
	{text = "There's a torn bit of paper sticking out of the sand. It looks like it was burnt from the firepit nearby."},
 	{ text = "It's part of a photograph of a really confused-looking baby, who is crawling on the ground. The baby appears to have something on their back." },
	{text = "It, uh, looks like it could be a costume, but I can't tell, because that part of the image is badly charred."},
 	{text = "There's also a date stamp on the photo. 13-1-93. That would be January 13, 1993. Is that very long ago? I can't really remember."},
 	{text = "I mean, it's such a strange photograph that I may as well take it with me."}
 }