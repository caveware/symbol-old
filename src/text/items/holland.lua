--[[
	Text when picking up the photograph
]]

return {
	{ text = "It's, uh..." },
	{ text = "It's a missing flyer for a crudely-drawn dog. The dog is apparently called Holland." },
	{text = "The author didn't have a picture of their dog, so they drew one to the best of their limited ability on the flyer."},
 	{text = "A dog getting lost out in this desert is facing one of the harshest climates in the continent." },
	{text = "Let's face it, this unfortunate mutt is probably gone for good."},
	{text = "It makes this flyer feel a little pathetic... "},
 	{text = "I'm going to take this down."},
 }