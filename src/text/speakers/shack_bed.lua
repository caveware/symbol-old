--[[
	Bed in shack
]]

-- Sequence if you pick yes
local yessleep = {
	{
		flags = {
			sleep = "yes"
		},
		next = final_text,
		text = "You tuck yourself in the bed and try to get some sleep."
	}
}

-- Sequence if you pick no
local nosleep = {
	{
		flags = {
			sleep = "no"
		},
		next = final_text,
		text = "Now isn't the time for sleep."
	}
}

return {
	{
		choices = {
			{ text = "Yes", to = yessleep },
			{ text = "No", to = nosleep }
		},
		text = "Rest in this bed?"
	}
}