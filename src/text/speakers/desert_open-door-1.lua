--[[
	Open door in desert
]]

return {
	{
		italics = true,
		text = "You knock on the door.",
		sfx = "door_knock"
	},
	{
		text = "..."
	}
}