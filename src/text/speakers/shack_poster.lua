--[[
	Poster in shack
]]

return {
	{
		bg = {
			img = "poster",
			scale = 2
		},
		text = "It's a poster depicting the Death Valley National Park."
	},
	{ text = "Death Valley is a desert in California that boasts some of the most low-lying areas on the continent."},
	{ text = "It's apparently hosts of the most inhospitable climates in North America as well."},
	{ text = "Not too reassuring to be reminded of that."}
}