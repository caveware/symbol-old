--[[
	Leaving shack on first night
]]

return {
	{
		speaker = "murray",
		text = "Hey, where are you going?"
	},
	{
		speaker = "murray",
		text = "It's too cold and dark. There's nothing out for you there."
	}
}