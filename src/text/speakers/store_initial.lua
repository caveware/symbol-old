--[[
	Initial discussion at store
]]

return {
	{
		bg = {
			cut = true,
			img = "blackground"
		},
		speaker = "murray",
		text = "A customer! Been a long time since I've seen one of those. My name's Shel and this is the Ryan General Store."
	},
	{
		speaker = "murray",
		text = "Or at least, it was a general store. Now I just use it to store enchantments, you know, witchy skeleton stuff."
	},
	{
		speaker = "murray",
		text = "Wait, aren't you the dude trying to free folks from the soul's well? Murray told me about you."
	},
	{
		speaker = "murray",
		text = "I got a whole range of things that could make that a lot easier."
	},
	{
		speaker = "murray",
		text = "So let's make a deal."
	}
}