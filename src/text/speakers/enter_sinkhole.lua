--[[
	Enter the sinkhole
]]

-- Sequence if you pick yes
local yessinkhole = {
	{
		flags = {
			enter_sinkhole = "yes"
		},
		next = final_text,
		text = "You climb down into the void."
	}
}

-- Sequence if you pick no
local nosinkhole = {
	{
		flags = {
			enter_sinkhole = "no"
		},
		next = final_text,
		text = "You decide not to climb down."
	}
}

return {
	{
		choices = {
			{ text = "Yes", to = yessinkhole },
			{ text = "No", to = nosinkhole }
		},
		text = "Enter the sinkhole?"
	}
}