--[[
	Stone in overlook
]]

return {
	{
		bg = {
			img = "sign",
			scale = 2
		},
		text = "So this road seems to head to the Badwater site, but also a town called Ryan that lies up ahead."
	},
	{ text = "Except some idiot scrawled the paint out of the name 'Ryan', and replaced it with the word 'Hell' above."},
	{ text = "Seems like an awfully edgy thing to do to a sign, really."}
}