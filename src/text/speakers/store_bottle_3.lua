--[[
	Bottle 3 in store
]]

-- Sequence if you pick yes and can buy
local buy = {
	code = function ()
		if Game.shards < 400 then
			return {{
				speaker = "murray",
				text = "You simply do not have enough shards, SIR."
			}}
		end

		Game.shards = Game.shards - 400
		table.insert(Game.inventory, "bottle_3")

		Save:save()

		return {{
			speaker = "murray",
			text = "TAKE IT AND LEAVE!"
		}}
	end
}

-- Sequence if you pick no
local nobuy = {
	{
		speaker = "murray",
		text = "Oh dude you didn't buy it."
	}
}

return {
	{
		bg = {
			cut = true,
			img = "blackground"
		},
		speaker = "murray",
		choices = {
			{ text = "Yes", to = buy },
			{ text = "No", to = nobuy }
		},
		text = "Buy this?"
	}
}