--[[
	Closed door in desert
]]

return {
	{
		italics = true,
		text = "You knock on the door.",
		sfx = "door_knock"
	},
	{
		text = "..."
	},
	{
		text = "Huh. Nobody there. Guess I better keep looking around."
	}
}