--[[
	Unavailable at store
]]

return {
	{
		bg = {
			cut = true,
			img = "blackground"
		},
		speaker = "murray",
		text = "You already own that, don't be greedy."
	}
}