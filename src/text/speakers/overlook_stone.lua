--[[
	Stone in overlook
]]

return {
	{
		bg = {
			img = "cave_01",
			scale = 2
		},
		text = "It looks like someone has carved an image into this stone."
	},
	{ text = "It seems to depict a figure who is falling between two spaces, perhaps through a hole. Underneath them, there are a number of faceless figures watching the person fall."},
	{ text = "I wonder how long ago this particular marking was made?"}
}