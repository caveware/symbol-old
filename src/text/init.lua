--[[
	Loads all the text files for the game
]]

return {
	dream = require(... .. '/dreams'),
	intro = require(... .. '/sequences/intro'),
	items = {
		photograph = require(... .. '/items/photograph'),
		postcard = require(... .. '/items/postcard'),
		holland = require(... .. '/items/holland'),
		slip = require(... .. '/items/slip')
	},
	speakers = {
		desert_closedDoor = require(... .. '/speakers/desert_closed-door'),
		desert_edge = require(... .. '/speakers/desert_edge'),
		desert_openDoor1 = require(... .. '/speakers/desert_open-door-1'),
		desert_openDoor2 = require(... .. '/speakers/desert_open-door-2'),
		enter_sinkhole = require(... .. '/speakers/enter_sinkhole'),
		hell_sign = require(... .. '/speakers/hell_sign'),
		overlook_stone = require(... .. '/speakers/overlook_stone'),
		shack_bed = require(... .. '/speakers/shack_bed'),
		shack_firstNightLeave = require(... .. '/speakers/shack_first-night-leave'),
		shack_poster = require(... .. '/speakers/shack_poster'),
		store_bottle_1 = require(... .. '/speakers/store_bottle_1'),
		store_bottle_2 = require(... .. '/speakers/store_bottle_2'),
		store_bottle_3 = require(... .. '/speakers/store_bottle_3'),
		store_bottle_4 = require(... .. '/speakers/store_bottle_4'),
		store_bottle_5 = require(... .. '/speakers/store_bottle_5'),
		store_initial = require(... .. '/speakers/store_initial'),
		store_unavailable = require(... .. '/speakers/store_unavailable'),
		well_death = require(... .. '/speakers/well_death'),
		well_timeUp = require(... .. '/speakers/well_time-up')
	}
}