--[[
	Supplementary scripts for the sinkhole
]]

local SinkholeScripts = {}

function SinkholeScripts:enter() end

function SinkholeScripts:update(dt)
	if Game.flags.enter_sinkhole == "yes" then
		Game.flags.enter_sinkhole = nil

		-- Fade back in
		Timer.tween(1, self, {
			fade = 0
		}, "in-out-sine", function ()
			self.fade = 0
			Gamestate.switch(self, "well")
		end)
	end
end

return SinkholeScripts