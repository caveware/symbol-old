--[[
	Supplementary scripts for the overlook
]]

local OverlookScripts = {}

function OverlookScripts:enter() end

function OverlookScripts:update(dt)
	if (self.player.position.x <= 680) then

		-- Prevent player movement
		self.player.animation_phase = 0
		self.player.position.x = 680.1
		self.prevent_movement = true

		-- Move view and animate sequence
		self.view.x_offset = 0
		Timer.tween(1.5, self.view, {
			x_offset = -240
		}, 'out-sine', function ()
			Timer.after(1, function ()
				Timer.tween(1.5, self.view, {
					x_offset = 0
				}, 'out-sine', function ()
					self.view.x_offset = 0
					self.prevent_movement = false
				end)
			end)
		end)
	end
end

function OverlookScripts:draw() end

return OverlookScripts