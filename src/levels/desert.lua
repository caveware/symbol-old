--[[
	Supplementary scripts for the desert
]]

local DesertScripts = {}

function DesertScripts:enter()
	self.__murray_timer = nil
end

function DesertScripts:haveItems()
	return (_.find(Game.inventory, "photograph") and _.find(Game.inventory, "postcard"))
end

function DesertScripts:metMurray()
	return Game.flags.met_murray
end

function DesertScripts:interpretItem(item)
	if not item then return end

	if item.name == "shack_door" then
		-- If you have talked to Murray, just return self
		if DesertScripts:metMurray() then
			return item
		elseif DesertScripts:haveItems() then
			self.prevent_movement = true
			self.__after_text = false
			self.__state = "door_opened"
			return {
				type = "speaker",
				name = "desert_openDoor1"
			}
		else
			return {
				type = "speaker",
				name = "desert_closedDoor"
			}
		end
	end

	return item
end

function DesertScripts:update(dt)
	-- If door was just opened...
	if self.__after_text then
		if self.__state == "door_opened" then
			if not self.__murray_timer then
				sfx.door_open:play()
				self.__murray_timer = 0
				Timer.during(2, function (dt)
					if self.__murray_timer < .5 and self.__murray_timer + (dt / 2) >= .5 then
						love.timer.sleep(1)
					end
					self.__murray_timer = self.__murray_timer + dt / 2
				end, function ()
					self.__murray_timer = 1
					self.__state = "door_closed"
					self.open_text = txt.speakers.desert_openDoor2
					self.__after_text = false
				end)
			end

			return
		elseif self.__state == "door_closed" then
			self.player.direction.x = 0
			self.player.direction.y = -1
			if self.fade == 1 then
				Timer.tween(.5, self, {
					fade = 0
				}, 'in-out-sine', function ()
					Game.flags.met_murray = true
					self.__state = nil
					self.__murray_timer = nil
					self.prevent_movement = false
					Gamestate.switch(self, "shack", "door_open")
				end)
			end

			self.player.position.y = self.player.position.y - dt * 20
			self.player.animation_phase = (self.player.animation_phase + dt * 80) % 2

			return
		end
	end

	if self.fade < 1 then return end

	if self.player.position.x < 200 then
		if self.showed_edge then
			self.showed_edge = false

			Timer.tween(.5, self, {
				fade = 0
			}, 'in-out-sine', function ()
				self.player.position.x = 300

				Timer.tween(.5, self, {
					fade = 1
				}, 'in-out-sine', function ()
					self.fade = 1
				end)
			end)
		else
			self.showed_edge = true
			self.open_text = txt.speakers.desert_edge
		end
	end
end

function DesertScripts:draw()
	love.graphics.setColor(255, 255, 255)

	if self.__murray_timer ~= nil then
		love.graphics.draw(gfx.desert.door_open, 956, 176)
		DesertScripts:drawMurray(self, 956, 176, self.__murray_timer)
		love.graphics.setColor(255, 255, 255)
	else
		love.graphics.draw(gfx.desert.door_closed, 995, 176)
	end

	self.__after_text = true
end

function DesertScripts:drawMurray(self, x, y, timer)
	local alpha = math.clamp(0, timer * 2, 1)
	local step = alpha == 1 and math.floor((timer * 10) % 2) + 1 or 1

	local offset = (timer < .5
		and 22 + timer * 12
		or 33 * (timer - .5) + 28)

	love.graphics.setColor(255, 255, 255, alpha * 255)
	love.graphics.setScissor((x - self.view.x) * 2, 0, 154, 600)
	love.graphics.draw(gfx.murray["left" .. step], x + 88 - offset, y + 14)
	love.graphics.setScissor()
end

return DesertScripts