--[[
	Supplementary scripts for the well
]]

-- Classes
local LostSoul = require("src.classes.LostSoul")

local WellScripts = {}

function WellScripts:enter()
	-- How much time will be spent in the well
	self.__timer = 50

	-- All spawn points for enemies
	self.spawns = self.map.layers.spawn.objects
	self.map:removeLayer("spawn")

	-- All enemies
	self.enemies = {}

	self.hide_player = true
end

function WellScripts:endLevel()
	Timer.tween(1, self, {
		fade = 0
	}, 'in-out-sine', function ()
		self.fade = 0

		-- Update the night
		Game.night = Game.night + 1
		NC:send("Night " .. Game.night)
		Gamestate.switch(self, "shack")
	end)
end

function WellScripts:killEnemies()
	self.enemies = {}
end

function WellScripts:spawnEnemies()
	local ox, oy = self.selected.x, self.selected.y
	local ow, oh = self.selected.width, self.selected.height

	for i = 0, 100 do
		local x, y
		repeat
			x, y = ox + math.random() * ow, oy + math.random() * oh
		until self.player.position:dist(Vector(x, y)) > 256
		table.insert(self.enemies, LostSoul(Vector(x, y)))
	end
end

function WellScripts:update(dt)
	if self.fade ~= 1 then return end

	local x, y = self.player.position:unpack()
	local position = Vector(x + 16, y)

	-- After text
	if self.__timer == 0 then
		WellScripts.endLevel(self)
		return
	end

	-- Decrease timer
	self.__timer = math.max(0, self.__timer - dt)

	-- Leave if timer is 0
	if self.__timer == 0 then self.open_text = txt.speakers.well_timeUp end

	self.previous = self.selected
	self.selected = Hit.checkRectangleTable(self.player:toHitbox(), self.spawns)
	if self.selected ~= self.previous then
		if not self.selected then
			WellScripts.killEnemies(self)
		else
			WellScripts.spawnEnemies(self)
		end
	end

	-- Handle damage
	local val = self.player:isInvincible() and -4 or 1
	for k, enemy in ipairs(self.enemies) do
		enemy:update(dt * val, position)
		if enemy.position:dist(position) < 16 then
			if not self.player:inflictDamage() and not self.player:isInvincible() then
				sfx.hit:play()
				self.__timer = 0
				self.open_text = txt.speakers.well_death
			end
		end
	end
end

function WellScripts:draw()
	-- Add enemies and player to table and sort it
	local drawables = _.append(self.enemies, { self.player })
	table.sort(drawables, function (a, b) return a.position.y < b.position.y end)

	-- Draw all drawable items
	for k, drawable in ipairs(drawables) do drawable:draw(dt) end
end

function WellScripts:drawFG()
	-- Draw timer
	local time = Format.asTimer(math.ceil(self.__timer))
	love.graphics.setFont(fnt.timer)
	love.graphics.setColor(0, 0, 0)
	love.graphics.printf(time, 2, 22, 800, 'center')
	love.graphics.setColor(255, 255, 255)
	love.graphics.printf(time, 0, 20, 800, 'center')
end

return WellScripts