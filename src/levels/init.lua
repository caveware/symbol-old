--[[
	Supplementary scripts
]]

return {
	desert = require(... .. '/desert'),
	overlook = require(... .. '/overlook'),
	shack = require(... .. '/shack'),
	sinkhole = require(... .. '/sinkhole'),
	store = require(... .. '/hell'),
	well = require(... .. '/well')
}