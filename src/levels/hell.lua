--[[
	Scripts for hell
]]

local HellScripts = {}

function HellScripts:enter() end

function HellScripts:interpretItem(item)
	if not item then return end

	-- Open store
	if item.name == "hell_store" then
		self.open_store = true
		return
	end

	return item
end

function HellScripts:update(dt) end

function HellScripts:draw()
	if self.open_store then
		self.open_store = nil
		Gamestate.push(states.Store)
	end
end

return HellScripts