--[[
	Supplementary scripts for the shack
]]

local ShackScripts = {}

function ShackScripts:enter()
	self.__glyph = RandomGlyph()
	self.__glyph_timer = 0
end

function ShackScripts:interpretItem(item)
	if not item then return end

	-- Do not allow leaving on first night
	if item.name == "door" then
		if Game.night == 1 then
			return {
				type = "speaker",
				name = "shack_firstNightLeave"
			}
		end
	end

	return item
end

function ShackScripts:sleep()
	-- Fade out and show dream
	Timer.tween(1, self, {
		fade = 0
	}, "in-out-sine", function ()
		self.fade = 0
		self.__after_text = false
		self.__increase_night = true

		self.open_text = txt.dream[math.floor(#txt.dream * math.random()) + 1]
	end)
end

function ShackScripts:update(dt)
	if Game.flags.sleep == "yes" then
		Game.flags.sleep = nil
		ShackScripts.sleep(self)
	end

	if self.__after_text and self.__increase_night then
		-- Update night
		self.__increase_night = false
		Game.night = Game.night + 1
		NC:send("Night " .. Game.night)

		-- Fade back in
		Timer.tween(1, self, {
			fade = 1
		}, "in-out-sine", function ()
			self.fade = 1
		end)
	end

	self.__glyph_timer = (self.__glyph_timer + dt)
	if self.__glyph_timer > 1 then
		self.__glyph_timer = 0
		self.__glyph:update()
	end
end

function ShackScripts:drawFG()
	love.graphics.setColor(255, 255, 255)
	self.__after_text = true
	if Gamestate.current() ~= self and self.fade == 0 then
		love.graphics.scale(2, 2)
		self.glyph:draw(184, 134)
		love.graphics.scale(.5, .5)
	end
end

return ShackScripts