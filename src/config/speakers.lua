--[[
	All speakers
]]

return {
	murray = {
		icon = {
			gfx.murray.icon1,
			gfx.murray.icon2
		},
		name = "Murray"
	}
}