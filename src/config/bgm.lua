--[[
	Which BGM files will match to which levels
]]

return {
	sinkhole = 'badwater',
	cave = 'desert',
	desert = 'desert',
	overlook = 'overlook',
	shack = 'desert',
	store = 'desert',
	well = 'desert',
	shack_2 = 'store'
}