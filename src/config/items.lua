--[[
	Item definitions
]]

return {
	holland = {
		description = "A crudely-drawn flyer for a lost dog called Holland.",
		name = "Lost Dog"
	},
	photograph = {
		description = "The charred remains of a photograph of a baby.",
		name = "Photograph"
	},
	slip = {
		description = "A slip of paper advertising a general store in a town called Ryan.",
		name = "Store Slip"
	},
	postcard = {
		description = "An old postcard advertising the vistas of Death Valley.",
		name = "Postcard"
	},
	bottle_1 = {
		description = "A tonic for use in the well. When used, it pushes ghosts away in one direction.",
		in_store = true,
		name = "Tonic",
		power = true
	},
	bottle_2 = {
		description = "An elixir for use in the well. When used, it pushes ghosts away in all directions.",
		in_store = true,
		name = "Elixir",
		power = true
	},
	bottle_3 = {
		description = "A salve for use in the well. When used, it momentarily increases the speed of the user.",
		in_store = true,
		name = "Salve",
		power = true
	},
	bottle_4 = {
		description = "A lotion for use in the well. When used, it temporarily makes the user immunte to attack.",
		in_store = true,
		name = "Lotion",
		power = true
	},
	bottle_5 = {
		description = "A tincture for use in the well. When used, it boosts the user's movement in one direction.",
		in_store = true,
		name = "Tincture",
		power = true
	}
}
