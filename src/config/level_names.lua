--[[
	Level titles
]]

return {
	cave = "Cave",
	desert = "Death Valley",
	overlook = "Dante's View",
	shack = "Murray's Shack",
	sinkhole = "Badwater",
	store = "Hell",
	well = "Soul's Well",
	shack_2 = "Shack"
}