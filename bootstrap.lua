--[[
	Loads all required files and settings for the game
]]

-------------
-- GENERAL --
-------------

-- Load all generic libraries
require "lib"

-- Disable audio if flag says too
if DISABLE_AUDIO then
	love.audio.setVolume(0)
end

-- Load level names
LEVEL_NAMES = require "src.config.level_names"

-----------
-- AUDIO --
-----------

-- Load background music
bgm = require "bgm"

-- Load sound effects
sfx = require "sfx"

--------------
-- CONTROLS --
--------------

-- Loads the Android control scheme
local android = require "src.classes.AndroidControls"

-- Loads the control scheme
local controls = require "controls"

-- Creates the input manager
Input = love.system.getOS() == "Android"
	and android(controls)
	or Baton.new(controls)

----------
-- SAVE --
----------

-- Create save manager
SaveManager = require "src.classes.SaveManager"

------------
-- STATES --
------------

-- Load all states
states = require "src.states"

-- Load level scripts
levels = require "src.levels"

------------
-- VISUAL --
------------

-- Sets the graphical filter to nearest
love.graphics.setDefaultFilter("nearest")

-- Loads all fonts
fnt = require "fnt"

-- Load all graphics
gfx = require "gfx"

-- Load all text
txt = require "src.text"

-- Load notification centre
NotificationCentre = require "src.classes.NotificationCentre"

-- Load the random glyph
RandomGlyph = require "src.classes.RandomGlyph"

-- Speakers
speakers = require "src.config.speakers"