--[[
	Configuration file
]]

require "constants"

function love.conf(t)
	-- Development config
	t.console = (not PRODUCTION)

	-- Game identity
	t.identity = "Caveware_SYMBOL"

	-- Window config
	t.window.height = GAME_HEIGHT
	t.window.minheight = GAME_HEIGHT
	t.window.minwidth = GAME_WIDTH
	t.window.resizable = true
	t.window.title = "Symbol"
	t.window.width = GAME_WIDTH
	t.window.vsync = false
end