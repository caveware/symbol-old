--[[
	Controls for SYMBOL
]]

return {
	a = {
		'key:return',
		'key:space',
		'key:z',
		'button:a'
	},
	b = {
		'key:backspace',
		'key:x',
		'button:b'
	},
	x = {
		'button:x'
	},
	y = {
		'key:i',
		'key:tab',
		'button:y'
	},
	down = {
		'key:down',
		'key:s',
		'axis:lefty+',
		'button:dpdown'
	},
	left = {
		'key:a',
		'key:left',
		'axis:leftx-',
		'button:dpleft'
	},
	right = {
		'key:d',
		'key:right',
		'axis:leftx+',
		'button:dpright'
	},
	up = {
		'key:up',
		'key:w',
		'axis:lefty-',
		'button:dpup'
	},
	pause = {
		'key:escape',
		'button:start'
	},
	f11 = {
		'key:f11'
	}
}
