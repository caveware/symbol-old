--[[
	Entry point for SYMBOL
]]

-- Load the bootstrap
require "bootstrap"

-- Performed upon game load
function love.load()
	-- Set the game to be fullscreen based on constant
	love.window.setFullscreen(GAME_FULLSCREEN)

	-- Contains all data required to keep the game in-sync save-wise
	Game = {
		flags = { },
		inventory = {
			"bottle_2",
			"bottle_3",
			"bottle_4"
		},
		level = START_LEVEL,
		levels = {
			"cave",
			"desert",
			"overlook",
			"sinkhole",
			"store"
		},
		night = 1,
		powers = {},
		shards = 0,
		version = '1'
	}

	-- Create notification centre
	NC = NotificationCentre()

	-- Create save manager
	Save = SaveManager()

	-- Set up function wrappers for states
	Gamestate.registerEvents()

	-- Set the starting state
	if START_IN_LEVEL then
		Gamestate.switch(states.Wandering, Game.level)
	else
		Gamestate.switch(states.CDSplash)
	end

	-- Wrap update function to restrict delta time
	local oldUpdateFn = love.update
	love.update = function (dt) oldUpdateFn(math.min(dt, DT_LIMIT)) end

	-- Wrap draw function to scale canvas
	local oldDrawFn = love.draw
	love.draw = function ()
		love.drawScaled(function ()
			oldDrawFn()

			-- Draw the save icon
			love.graphics.origin()
			love.graphics.setColor(255, 255, 255, 255 * Save.timer)
			love.graphics.draw(gfx.ui.saving, 704, 32)

			-- Draw notifications
			NC:draw()

			-- Draw debug info if in development
			if not PRODUCTION then
				love.graphics.setColor(255, 255, 255)
				love.graphics.setFont(fnt.text)
				love.graphics.print("DEBUG:\nFPS: " .. love.timer.getFPS(), 16, 16)
			end
		end)

		-- Draw debug if on Android
		if love.system.getOS() == "Android" then
			Input:draw()
		end
	end
end

-- Performed each update
function love.update(dt)
	-- Run the debug web server if not in production
	if not PRODUCTION then Lovebird:update() end

	-- Update the control scheme with joystick, if found
	local joysticks = love.joystick.getJoysticks()
	if #joysticks > 0 then Input.joystick = joysticks[1] end

	-- Update the BGM
	bgm:update(dt)

	-- Update the controls
	Input:update()

	-- Update the notifications
	NC:update(dt)

	-- Update the save manager
	Save:update()

	-- Update the timer
	Timer.update(dt)

	-- Check for F11
	if Input:pressed(KEY_F11) then
		love.window.setFullscreen(not love.window.getFullscreen())
	end
end