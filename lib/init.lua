--[[
	Game libraries
]]

-- HUMP libraries
Camera = require 'lib.hump.camera'
Class = require 'lib.hump.class'
Gamestate = require 'lib.hump.gamestate'
Signal = require 'lib.hump.signal'
Timer = require 'lib.hump.timer'
Vector = require 'lib.hump.vector'

-- Caveware Digital libraries
require 'lib.caveware'

-- Other libraries
_ = require 'lib.moses'
Baton = require 'lib.baton'
JSON = require 'lib.json'
Lovebird = require 'lib.lovebird'
STI = require 'lib.sti'
