--[[
	Functions relating to scaling of the game
]]

-- Canvas which unscaled game will be drawn to
local canvas = love.graphics.newCanvas(GAME_WIDTH, GAME_HEIGHT)

__GLOBAL_CANVAS = canvas

-- Call function to draw scaled game
function love.drawScaled(drawFn)
	-- Draw given function to canvas
	love.graphics.setCanvas(canvas)
		love.graphics.clear()
		drawFn()
	love.graphics.setCanvas()

	-- Determine window size
	local window_height = love.graphics.getHeight()
	local window_width = love.graphics.getWidth()

	-- Determine scaling values
	local scale_h = window_width / GAME_WIDTH
	local scale_v = window_height / GAME_HEIGHT
	local scale = math.min(scale_h, scale_v)

	-- Determine x and y of canvas
	local x, y = 0, 0
	if (scale_h > scale_v) then
		x = (window_width - GAME_WIDTH * scale) / 2
	else
		y = (window_height - GAME_HEIGHT * scale) / 2
	end

	-- Draw canvas on screen with calculated position and size
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(canvas, x, y, 0, scale)
end