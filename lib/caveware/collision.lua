-- Handles checking AABB collisions

local Hit = {}

function Hit.checkRectangles(rect1, rect2)
	return
		rect1.x < rect2.x + rect2.width and
		rect1.y < rect2.y + rect2.height and
		rect2.x < rect1.x + rect1.width and
		rect2.y < rect1.y + rect1.height
end

function Hit.checkRectangleTable(rect, rect_table)
	local found = nil
	for z, rect_a in ipairs(rect_table) do
		if Hit.checkRectangles(rect, rect_a) then
			found = rect_a
		end
	end
	return found
end

function Hit.moveByPath(dt, object, objects)
	local point = object.path[(object.point % #object.path) + 1]
	local vector = Vector(point.x - object.x, point.y - object.y):toPolar()
	local angle, dist = vector.x, vector.y
	local original_dist = dist

	-- Move player closer to point
	dist = dist - object.speed * dt

	-- If player will end up on point, move to point and progress
	if (dist <= 0) then
		object.x = point.x
		object.y = point.y
		object.point = (object.point % #object.path) + 1
		vector = Vector(point.x - object.x, point.y - object.y):toPolar()
		angle, dist = vector.x, vector.y
	end

	-- Movement along angle compensating for dist
	vector = Vector.fromPolar(angle, dist)
	object.x = point.x - vector.y
	object.y = point.y - vector.x

	-- Collision detection
	while (Hit.checkRectangleTable(object, objects) and dist < original_dist) do
		dist = math.min(dist + 1, original_dist)
		vector = Vector.fromPolar(angle, dist)
		object.x = point.x - vector.y
		object.y = point.y - vector.x
	end

	-- Update the facing direction of the object
	object.facing = Vector(vector.y, vector.x):normalized()
end

function Hit.moveByVelocity(dt, object, objects)
	local detection

	-- Move physics object horizontally
	object.x = object.x + object.velocity.x * dt
	detection = Hit.checkRectangleTable(object, objects)
	if (detection) then
		object.x = object.velocity.x > 0
			and detection.x - object.width
			or detection.x + detection.width
		object.velocity.x = 0
	end

	-- Move physics object vertically
	object.y = object.y + object.velocity.y * dt
	detection = Hit.checkRectangleTable(object, objects)
	if (detection) then
		object.y = object.velocity.y > 0
			and detection.y - object.height
			or detection.y + detection.height
		object.velocity.y = 0
	end
end

function Hit.performMovement(dt, objects)
	for key, object in ipairs(objects) do
		-- Do not attempt to move inactive objects
		if (object.active) then
			-- Find all objects other than this one
			local other_objects = _.reject(objects, function (index)
				return index == key
			end)

			-- Are we moving along path?
			if (object.path) then
				Hit.moveByPath(dt, object, other_objects)
			else
				Hit.moveByVelocity(dt, object, other_objects)
			end
		end
	end
end

return Hit