--[[
	String formatting library
]]

local Format = {}

function Format.asTimer(time)
	local mins = math.floor(time / 60)
	local secs = time % 60
	return Format.withLeadingZero(mins) .. ":" .. Format.withLeadingZero(secs)
end

function Format.withLeadingZero(value)
	return (value < 10) and ("0" .. value) or value
end

return Format