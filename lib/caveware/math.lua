--[[
	Extra maths functions
]]

-- Seed the random number generator
math.randomseed(os.time())

-- Restricts the value to be inside of the given range
function math.clamp(min, value, max)
	return math.min(max, math.max(value, min))
end

-- Returns the distance betweeen two points
function math.dist(x1, y1, x2, y2)
	return ((x2 - x1) ^ 2 + (y2 - y1) ^ 2) ^ 0.5
end

-- Rounds the value given
function math.round(value)
	return math.floor(value + .5)
end

-- Returns the sign of a given value
function math.sign(value)
	return value > 0 and 1 or (value < 0 and -1 or 0)
end
