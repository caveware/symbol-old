local gfx = {}

-- Desert
gfx.desert = {
	door_closed = love.graphics.newImage(... .. '/desert/door_closed.png'),
	door_open = love.graphics.newImage(... .. '/desert/door_open.png')
}

-- Dialog
gfx.dialog = {
	bg = {
		blackground = love.graphics.newImage(... .. '/dialog/bg/blackground.png'),
		blank = love.graphics.newImage(... .. '/dialog/bg/blank.png'),
		red = love.graphics.newImage(... .. '/dialog/bg/red.png'),
		intro1 = love.graphics.newImage(... .. '/intro/intro_1.png'),
		intro2 = love.graphics.newImage(... .. '/intro/intro_2.png'),
		poster = love.graphics.newImage(... .. '/dialog/bg/poster.png'),
		cave_01 = love.graphics.newImage(... .. '/dialog/bg/cave_01.png'),
		sign = love.graphics.newImage(... .. '/dialog/bg/sign.png')
	}
}

-- Environment
gfx.env = {
	concrete = love.graphics.newImage(... .. '/environment/concrete.png')
}

-- Items
gfx.items = {
	photograph = love.graphics.newImage(... .. '/items/photograph.png'),
	postcard = love.graphics.newImage(... .. '/items/postcard.png'),
	holland = love.graphics.newImage(... .. '/items/holland.png'),
	slip = love.graphics.newImage(... .. '/items/slip.png')
}

-- Inventory
gfx.inventory = {
	cursor = love.graphics.newImage(... .. '/inventory/cursor.png'),
	holland = love.graphics.newImage(... .. '/inventory/holland.png'),
	photograph = love.graphics.newImage(... .. '/inventory/photograph.png'),
	postcard = love.graphics.newImage(... .. '/inventory/postcard.png'),
	slip = love.graphics.newImage(... .. '/inventory/slip.png'),
	ui = love.graphics.newImage(... .. '/inventory/ui.png'),
	bottle_1 = love.graphics.newImage(... .. '/inventory/bottle_1.png'),
	bottle_2 = love.graphics.newImage(... .. '/inventory/bottle_2.png'),
	bottle_3 = love.graphics.newImage(... .. '/inventory/bottle_3.png'),
	bottle_4 = love.graphics.newImage(... .. '/inventory/bottle_4.png'),
	bottle_5 = love.graphics.newImage(... .. '/inventory/bottle_5.png'),
	small_cursor = love.graphics.newImage(... .. '/inventory/small_cursor.png')
}

-- Map
gfx.map = {
	blank = love.graphics.newImage(... .. '/map/map_blank.png'),
	cursor = {
		selected = love.graphics.newImage(... .. '/map/cursor_selected.png'),
		selecting = love.graphics.newImage(... .. '/map/cursor_selecting.png')
	},
	icon = {
		cave = love.graphics.newImage(... .. '/map/icon_cave.png'),
		desert = love.graphics.newImage(... .. '/map/icon_desert.png'),
		overlook = love.graphics.newImage(... .. '/map/icon_overlook.png'),
		sinkhole = love.graphics.newImage(... .. '/map/icon_sinkhole.png'),
		store = love.graphics.newImage(... .. '/map/icon_store.png')
	}
}

-- Player
gfx.player = {
	{
		down1 = love.graphics.newImage(... .. '/player_not_damaged/down1.png'),
		down2 = love.graphics.newImage(... .. '/player_not_damaged/down2.png'),
		idle = love.graphics.newImage(... .. '/player_not_damaged/idle.png'),
		left1 = love.graphics.newImage(... .. '/player_not_damaged/left1.png'),
		left2 = love.graphics.newImage(... .. '/player_not_damaged/left2.png'),
		right1 = love.graphics.newImage(... .. '/player_not_damaged/right1.png'),
		right2 = love.graphics.newImage(... .. '/player_not_damaged/right2.png'),
		up1 = love.graphics.newImage(... .. '/player_not_damaged/up1.png'),
		up2 = love.graphics.newImage(... .. '/player_not_damaged/up2.png')
	}, {
		down1 = love.graphics.newImage(... .. '/player_slightly_damaged/down1.png'),
		down2 = love.graphics.newImage(... .. '/player_slightly_damaged/down2.png'),
		idle = love.graphics.newImage(... .. '/player_slightly_damaged/idle.png'),
		left1 = love.graphics.newImage(... .. '/player_slightly_damaged/left1.png'),
		left2 = love.graphics.newImage(... .. '/player_slightly_damaged/left2.png'),
		right1 = love.graphics.newImage(... .. '/player_slightly_damaged/right1.png'),
		right2 = love.graphics.newImage(... .. '/player_slightly_damaged/right2.png'),
		up1 = love.graphics.newImage(... .. '/player_slightly_damaged/up1.png'),
		up2 = love.graphics.newImage(... .. '/player_slightly_damaged/up2.png')
	}, {
		down1 = love.graphics.newImage(... .. '/player_very_damaged/down1.png'),
		down2 = love.graphics.newImage(... .. '/player_very_damaged/down2.png'),
		idle = love.graphics.newImage(... .. '/player_very_damaged/idle.png'),
		left1 = love.graphics.newImage(... .. '/player_very_damaged/left1.png'),
		left2 = love.graphics.newImage(... .. '/player_very_damaged/left2.png'),
		right1 = love.graphics.newImage(... .. '/player_very_damaged/right1.png'),
		right2 = love.graphics.newImage(... .. '/player_very_damaged/right2.png'),
		up1 = love.graphics.newImage(... .. '/player_very_damaged/up1.png'),
		up2 = love.graphics.newImage(... .. '/player_very_damaged/up2.png')
	}
}

-- Memory
gfx.memory = {
	down1 = love.graphics.newImage(... .. '/memories/down1.png'),
	down2 = love.graphics.newImage(... .. '/memories/down2.png'),
	idle = love.graphics.newImage(... .. '/memories/idle.png'),
	left1 = love.graphics.newImage(... .. '/memories/left1.png'),
	left2 = love.graphics.newImage(... .. '/memories/left2.png'),
	right1 = love.graphics.newImage(... .. '/memories/right1.png'),
	right2 = love.graphics.newImage(... .. '/memories/right2.png'),
	up1 = love.graphics.newImage(... .. '/memories/up1.png'),
	up2 = love.graphics.newImage(... .. '/memories/up2.png')
}

-- Menu
gfx.menu = {
	carving = love.graphics.newImage(... .. '/menu/carving.png'),
	text = love.graphics.newImage(... .. '/menu/text.png'),
	text_cont = love.graphics.newImage(... .. '/menu/text_cont.png')
}

-- Murray
gfx.murray = {
	icon1 = love.graphics.newImage(... .. '/murray/icon_1.png'),
	icon2 = love.graphics.newImage(... .. '/murray/icon_2.png'),
	left1 = love.graphics.newImage(... .. '/murray/left_1.png'),
	left2 = love.graphics.newImage(... .. '/murray/left_2.png'),
	right1 = love.graphics.newImage(... .. '/murray/right_1.png'),
	right2 = love.graphics.newImage(... .. '/murray/right_2.png')
}

-- Shadow
gfx.shadow = {
	down1 = love.graphics.newImage(... .. '/shadow/down1.png'),
	down2 = love.graphics.newImage(... .. '/shadow/down2.png'),
	idle = love.graphics.newImage(... .. '/shadow/idle.png'),
	left1 = love.graphics.newImage(... .. '/shadow/left1.png'),
	left2 = love.graphics.newImage(... .. '/shadow/left2.png'),
	right1 = love.graphics.newImage(... .. '/shadow/right1.png'),
	right2 = love.graphics.newImage(... .. '/shadow/right2.png'),
	up1 = love.graphics.newImage(... .. '/shadow/up1.png'),
	up2 = love.graphics.newImage(... .. '/shadow/up2.png')
}

-- Splash images
gfx.splash = {
	cd_bw = love.graphics.newImage(... .. '/splash/cd_bw.png'),
	cd_colour = love.graphics.newImage(... .. '/splash/cd_colour.png'),
	save = love.graphics.newImage(... .. '/splash/save.png')
}

-- Store images
gfx.store = {
	bottle_1 = love.graphics.newImage(... .. '/store/bottle_1.png'),
	bottle_2 = love.graphics.newImage(... .. '/store/bottle_2.png'),
	bottle_3 = love.graphics.newImage(... .. '/store/bottle_3.png'),
	bottle_4 = love.graphics.newImage(... .. '/store/bottle_4.png'),
	bottle_5 = love.graphics.newImage(... .. '/store/bottle_5.png'),
	cursor = love.graphics.newImage(... .. '/store/cursor.png'),
	ui = love.graphics.newImage(... .. '/store/ui.png'),
	unavailable = love.graphics.newImage(... .. '/store/unavailable.png')
}

-- UI
gfx.ui = {
	arrow = love.graphics.newImage(... .. '/ui/textbox_arrow.png'),
	glyphs = love.graphics.newImage(... .. '/ui/title_glyphs.png'),
	saving = love.graphics.newImage(... .. '/ui/saving.png'),
	symbols = love.graphics.newImage(... .. '/ui/symbols.png'),
	symbols2 = love.graphics.newImage(... .. '/ui/symbols2.png'),
	textbox = love.graphics.newImage(... .. '/ui/textbox.png'),
	textbox_image = love.graphics.newImage(... .. '/ui/textbox_image.png'),
	title = love.graphics.newImage(... .. '/ui/title.png'),
	vignette = love.graphics.newImage(... .. '/ui/vignette.png')
}

symbolQuads = {}

for y = 0, 3 do
	symbolQuads[y + 1] = {}

	for x = 0, 3 do
		table.insert(symbolQuads[y + 1], love.graphics.newQuad(x * 32, y * 32, 32, 32, 128, 128))
	end
end

return gfx